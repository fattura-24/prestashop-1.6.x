<?php
/**
 * Questo file è parte del plugin Prestashop v1.7.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Descrizione: elimina file di log
 */

$response = array('status'=>false);
$filePath = dirname(__FILE__) . '/trace.log';

if(file_exists($filePath))
{
    $response['fileExists'] = true;
    $response['size'] = round(filesize($filePath) / 1000.);
    unlink($filePath);
}
else
{
    $response['fileExists'] = false;
}

$response['path'] = $filePath;
$response['status'] = true;
echo json_encode($response);