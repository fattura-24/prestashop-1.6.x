<?php
/**
 * Questo file è parte del plugin Prestashop v1.7.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Descrizione: scarica file di log
 */
$data = $_POST['info'];
$file = dirname(__FILE__) . '/trace.log';
$fp = fopen($file , "a+");
fprintf($fp, "\n%s\n", $data);
fclose($fp);