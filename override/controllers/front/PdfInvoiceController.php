<?php
/**
 * Questo file è parte del plugin Prestashop v1.6.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Descrizione: Override del controller prestashop/controllers/front/PdfInvoiceController.php
 */

class PdfInvoiceController extends PdfInvoiceControllerCore
{    
    public function display()
    {
        if(!Module::isEnabled('fattura24') || Configuration::get('PS_CREAZIONE_FATTURA') == 0)
            return parent::display();   

        $fileName = Module::getInstanceByName('fattura24')->constructFileName($this->order->invoice_number,$this->order->date_add);
        $filePath = _PS_DOWNLOAD_DIR_ . 'Fattura24/' . $fileName;
        
        if(!file_exists($filePath) || filesize($filePath) < 200)
        {
            $orderId = $this->order->id;
            $query = 'SELECT docIdFattura24 FROM ' . _DB_PREFIX_ . 'orders WHERE id_order=\'' . $orderId . '\';';
            $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];

            if(!empty($docIdFattura24)){ 
                Module::getInstanceByName('fattura24')->downloadDocument($docIdFattura24, $orderId, true);
            }
        }

        if(file_exists($filePath) && filesize($filePath) > 200)
        {
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="' . $fileName. '"');
            header('Content-Transfer-Encoding: binary');
        	readfile($filePath);
        }
        else
            return parent::display();
    }
}
