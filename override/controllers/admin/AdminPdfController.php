<?php
/**
 * Questo file è parte del plugin Prestashop v1.6.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Descrizione: Override del controller prestashop/controllers/admin/AdminPdfController.php
 */

class AdminPdfController extends AdminPdfControllerCore
{
    public function generateInvoicePDFByIdOrder($orderId)
    {
        if(!Module::isEnabled('fattura24') || Configuration::get('PS_CREAZIONE_FATTURA') == 0)
            return parent::generateInvoicePDFByIdOrder($orderId);

        $order = new Order((int)$orderId);
        if (!Validate::isLoadedObject($order)) {
            die(Tools::displayError('The order cannot be found within your database.'));
        }

        $fileName = Module::getInstanceByName('fattura24')->constructFileName($order->invoice_number,$order->date_add);
        $filePath = _PS_DOWNLOAD_DIR_ . 'Fattura24/' . $fileName;

        if(!file_exists($filePath) || filesize($filePath) < 200)
        {
            $query = 'SELECT docIdFattura24 FROM ' . _DB_PREFIX_ . 'orders WHERE id_order=\'' . $orderId . '\';';
            $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];

            if(!empty($docIdFattura24))
                Module::getInstanceByName('fattura24')->downloadDocument($docIdFattura24, $orderId, true);
        }

        if(file_exists($filePath) && filesize($filePath) > 200)
        {
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="' . $fileName. '"');
            header('Content-Transfer-Encoding: binary');
        	readfile($filePath);
        }
        else
            return parent::generateInvoicePDFByIdOrder($orderId);
    }
    
    public function generateInvoicePDFByIdOrderInvoice($id_order_invoice)
    {

        if(!Module::isEnabled('fattura24') || Configuration::get('PS_CREAZIONE_FATTURA') == 0)
            return parent::generateInvoicePDFByIdOrderInvoice($id_order_invoice);
        
        $order_invoice = new OrderInvoice((int)$id_order_invoice);
        if (!Validate::isLoadedObject($order_invoice)) {
            die(Tools::displayError('The order invoice cannot be found within your database.'));
        }

        $orderId = $order_invoice->id_order;
        $order = new Order($orderId);

        $fileName = Module::getInstanceByName('fattura24')->constructFileName($order->invoice_number,$order->date_add);
        $filePath = _PS_DOWNLOAD_DIR_ . 'Fattura24/' . $fileName;

        if(!file_exists($filePath) || filesize($filePath) < 200)
        {
            $query = 'SELECT docIdFattura24 FROM ' . _DB_PREFIX_ . 'orders WHERE id_order=\'' . $orderId . '\';';
            $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];
        
            if(!empty($docIdFattura24))
                Module::getInstanceByName('fattura24')->downloadDocument($docIdFattura24, $orderId, true);
        }

        if(file_exists($filePath) && filesize($filePath) > 200)
        {
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="' . $fileName. '"');
            header('Content-Transfer-Encoding: binary');
        	readfile($filePath);
        }
        else
            return parent::generateInvoicePDFByIdOrderInvoice($id_order_invoice);
    }
}
