<div class="panel" id="Fattura24">
    <div class="panel-heading">
        <i class="icon-money"></i>{l s='Fattura24' mod='fattura24'}
        <span class="badge">{$order_documents}</span>
    </div>
        <table>
            <tbody>
                <thead>
                    <tr>
                        <td><strong>{l s='Stato ordine' mod='fattura24'}   </strong></td>
                        <td style="padding-left: 20px;"><strong>{l s='Stato fattura' mod='fattura24'}  </strong></td>
                    </tr>
                </thead>      
                
                <tr>
                    <td id="f24_order">{$fattura24_order}</td>
                    <td id="f24_invoice" style="padding-left: 20px;">{$fattura24_invoice}</td>
                </tr>
                
                <!--<tr>
                    <td>
                        {if $order_status_name neq '' && $fattura24_order_docId == '' && $config_CreaOrdine eq 1}  
                            <button type="submit" name="submit_order" id="submit_order" class="btn btn-primary">Salva Ordine</button>
                        {/if}
                    </td>
                    
                    <td style="padding-left: 20px;">
                        {if $order_status_name neq '' && $fattura24_invoice_docId == '' && $config_CreaFattura neq 0 } 
                            <button type="submit" name="submit_invoice" id="submit_invoice" class="btn btn-primary">Salva Fattura</button>
                        {/if}
                    </td>
                </tr>-->
            </tbody>
        </table>
    </div>
</div>   
<script type="text/javascript">
var orderbtn = document.getElementById('submit_order');

orderbtn.addEventListener('click', function (e){
    $.ajax({
        type : 'POST',
        data : 'data',
        url: 'fattura24';
        success : function (data){
            console.log(data);
        }
    });
});

</script>