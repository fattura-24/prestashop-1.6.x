{*
* Questo file è parte del plugin Prestashop v1.6.x di Fattura24
* Autore: Fattura24.com <info@fattura24.com> 
*
* Descrizione: con questo template aggiungo e gestisco i campi codice destinatario e pec al form di registrazione utente
*}
<input type="hidden" name="id_fattura24" value="{if isset($id_fattura24)}{$id_fattura24}{else}{/if}">
<input type="hidden" name="fattura24_id_customer" value="{$fattura24_id_customer}">
<div class="form-group row">
    <label>{l s='Indirizzo PEC' mod='fattura24'}</label>
    <div>
        <input type="email" name="fattura24_pec" value="{if isset($smarty.post.fattura24_pec)}{$smarty.post.fattura24_pec}{else}{$fattura24_pec}{/if}"/>
    </div>
</div>
<div class="form-group row">
    <label>{l s='Codice Destinatario' mod='fattura24'}</label>
    <div>
        <input type="text" maxlength="7" name="fattura24_codice_destinatario" value="{if isset($smarty.post.fattura24_codice_destinatario)}{$smarty.post.fattura24_codice_destinatario}{else}{$fattura24_codice_destinatario}{/if}"/>
    </div>
</div>
