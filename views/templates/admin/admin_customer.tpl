{*
* Questo file è parte del plugin Prestashop v1.6.x di Fattura24
* Autore: Fattura24.com <info@fattura24.com> 
*
* Descrizione: template per visualizzare i campi codice univoco e pec nel cliente lato admin
*}

<div class="panel">
    <div class="panel-heading"><i class="icon-user"></i> {$section['label']|escape:'htmlall':'UTF-8'}{l s='Dati Fattura Elettronica' mod='fattura24'}</div>
    <form id="customer_note" class="form-horizontal" action="{$linkController}" method="post" >
        <div class="row">
            <div class="form-wrapper">
                <input type="hidden" name="id_fattura24" value="{$id_fattura24}"></input>
                <span>{l s='Codice Destinatario' mod='fattura24'}<input type="text" name="fattura24_codice_destinatario" value="{$fattura24_codice_destinatario}" readonly></span>
                <span>{l s='PEC' mod='fattura24'}<input type="text" name="fattura24_pec" value="{$fattura24_pec}" readonly></span>
            
                <button type="button" onclick="input_readOnly(this)">{l s='Modify' mod='fattura24'}</button>                     
            </div>
            <div class="col-lg-12">
                <button type="submit" id="submitPecSdicode" class="btn btn-default pull-right" disabled="disabled">
                    <i class="icon-save"></i>
                    {l s='Save' mod='fattura24'}
                </button>
            </div>
        </div>
    </form>
</div>                
   
<script>
    function input_readOnly(obj){
        var PecSdicodeForm = obj.form;
        PecSdicodeForm['fattura24_codice_destinatario'].readOnly = false;
        PecSdicodeForm['fattura24_pec'].readOnly = false;
        document.getElementById("submitPecSdicode").disabled = false;  
    }
</script>