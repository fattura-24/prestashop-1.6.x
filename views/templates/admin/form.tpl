{*
* Questo file è parte del plugin Prestashop v1.6.x di Fattura24
* Autore: Fattura24.com <info@fattura24.com> 
*
* Descrizione: con questo template gestisco le impostazioni del modulo F24
*}
<style type="text/css"> 
   .checkbox {
        margin-left: 30px;
    }
    .help {
        margin-top: 9px;
    }
    .help > a {
        width: 15px;
        height: 15px;
        border-radius: 50%;
        background-color: #cccccc;
        color: #ffffff;
        padding: 0 5px 0 5px;
    }
    .help > a:focus {
        text-decoration: none;
    }
    .fattura24-field {
        margin-left: 30px;
    }
    .fattura24-label {
        margin-left: 20px;
        font-size: 12px !important;
    }
    .fattura24-comment {
        font-size: 13px;
        font-style: italic;
        color: grey;
        padding-left: 0.5em;
    }
    .fattura24-label-correctApiKey {
        font-size: 13px !important;
        color: green;
        padding-left: 1.5em;
        margin-top: 0.5em;
    }
    .fattura24-label-fe-info {
        font-size: 10px !important;
        color: red;
        padding-left: 1.5em;
        margin-top: 0.5em;
    }
    .fattura24-error {
        padding: 5px;
        padding-left: 10px;
        padding-right: 10px;
        border-radius: 5px;
        color: #fff;
        background-color: #ff6000;
    }
    .fattura24-warning {
        padding: 5px;
        padding-left: 10px;
        padding-right: 10px;
        border-radius: 5px;
        color: #fff;
        background-color: #ffbf00;
    }
    .field-icon {
        float: right;
        margin-left: -25px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
</style>

<form id="module_form" class="defaultForm form-horizontal"
      action="index.php?controller=AdminModules&configure=fattura24&tab_module=administration&module_name=fattura24&token={$token}"
      method="post" enctype="multipart/form-data" novalidate>
    <input type="hidden" name="submitModule" value="1"/>

    <div class="panel" id="fieldset_0">
        <div class="panel-heading">
            <i class="icon-cogs"></i> Fattura24
        </div>
           
        {if !$isFattura24Enabled}
            <div class="form-group" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <div class ="fattura24-error">
                        <h4>MODULO NON ATTIVATO</h4>
                    </div>
                </div>
            </div>
        {/if}

        {if $psDisableOverrides == 1}
            <div class="form-group" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <div class ="fattura24-error">
                        <h4>ATTENZIONE CONFIGURAZIONE ERRATA:</h4>
                        <h4>Per visualizzare i PDF delle fatture, devi impostare su 'No' la seguente impostazione: 
                        Parametri avanzati -> Prestazioni -> Disattiva tutti gli override</h4>
                    </div>
                </div>
            </div>
        {/if}

                <div class="form-wrapper">
                    <div class="form-group col-lg-12" style="margin-top:20px">
                        <h2>IMPOSTAZIONI GENERALI</h2>   
                        <hr>  
                        
                        <button type="submit" value="1" id="module_form_submit_btn_header" name="submitModule"
                            class="btn btn-default pull-right">
                        <i class="process-icon-save"></i> Salva
                        </button>
                    </div>
                </div>

                    <div class="form-group">
                        <label class="fattura24-label col-lg-2">Versione modulo</label>
                        <div class="col-lg-1">
                            <div class="fattura24-field">
                                <h4 id='version'>{$version}</h4>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="fattura24-label col-lg-2">
                            Api Key
                        </label>
                
                        <div class="fattura24-field col-lg-3">
                            <input type="text"
                                name="PS_FATTURA24_API"
                                id="PS_FATTURA24_API"
                                value="{$apiKey}"
                            />
                        </div>

                        <!--<div class="fattura24-field col-lg-4">
                                <input type="password"
                                    name="PS_FATTURA24_API"
                                    id="PS_FATTURA24_API"
                                    value="{$apiKey}"
                                />
                                <span class="icon-unlock field-icon" id="show_hide"></span>
                        </div>-->
                    <div>    
                        <input type="button" class="btn btn-default" id="verify" value="Verifica Api Key"/>&nbsp;&nbsp;
                        <span id="result">
                        </span>
                    </div>
                    <br />
                        
                    <label class="fattura24-label col-lg-2">
                        Scadenza abbonamento :
                    </label>
                    <div id="expire" class="fattura24-field col-lg-3">
                    </div>                    
                </div>
                <br/>

                <div class="form-group col-lg-12">
                    <h2>Rubrica</h2>
                    <hr>
                </div>
                    
                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Salva cliente
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo per salvare i dati del cliente nella rubrica di Fattura24 
                            quando viene creato un ordine in Prestashop o quando crei un documento (ordine o fattura) in Fattura24">?</a>
                        </span>
                    </label>
                    <div class="col-lg-1">
                        <div class="checkbox">
                            <label for="PS_SALVA_CLIENTE_">
                                <input type="checkbox" name="PS_SALVA_CLIENTE_"
                                    id="PS_SALVA_CLIENTE_" class="" value="1"
                                    {if $salvaCliente eq 1}
                                        checked="checked"
                                    {/if}
                                />
                            </label>
                        </div>
                    </div>
                </div>
                <br/>
            
                <div class="form-group col-lg-12">
                    <h2>Ordini</h2>
                    <hr>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Crea ordine&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi attivare la creazione dell'ordine in Fattura24">?</a>
                        </span>
                    </label>
                    <div class="col-lg-1">
                        <div class="checkbox">
                            <label for="PS_CREAZIONE_ORDINE_">
                                <input type="checkbox" name="PS_CREAZIONE_ORDINE_"
                                    id="PS_CREAZIONE_ORDINE_" class="" value="1"
                                    {if $creazioneOrdine eq 1}
                                        checked="checked"
                                    {/if}
                                />
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Scarica PDF&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi che venga scaricato automaticamente in locale il PDF dell'ordine">?</a>
                        </span>
                    </label>
                    <div class="col-lg-1">
                        <div class="checkbox">
                            <label for="PS_PDF_ORDINE_">
                                <input type="checkbox" name="PS_PDF_ORDINE_"
                                    id="PS_PDF_ORDINE_"
                                    value="1"
                                    {if $pdfOrdine eq 1}
                                        checked="checked"
                                    {/if}
                                />
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Invia email&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi che il PDF dell'ordine venga spedito automaticamente al cliente via email">?</a>
                        </span>
                    </label>
                    <div class="col-lg-1">
                        <div class="checkbox">
                            <label for="PS_MAIL_ORDINE_">
                                <input type="checkbox" name="PS_MAIL_ORDINE_"
                                    id="PS_MAIL_ORDINE_"
                                    value="1"
                                    {if $emailOrdine eq 1}
                                        checked="checked"
                                    {/if}
                                />
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Movimenta magazzino&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi attivare la movimentazione del magazzino tramite la creazione degli ordini. 
                            I prodotti in Fattura24 che hanno lo stesso codice dei prodotti di Prestashop saranno impegnati">?</a>
                        </span>
                    </label>
                    <div class="col-lg-1">
                        <div class="checkbox">
                            <label for="PS_F24_MAGAZZINO_ORDINE_">
                                <input type="checkbox" name="PS_F24_MAGAZZINO_ORDINE_"
                                    id="PS_F24_MAGAZZINO_ORDINE_" class="" value="1"
                                    {if $magazzinoOrdine eq 1}
                                        checked="checked"
                                    {/if}
                                />
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Modello ordine&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Seleziona il modello da usare per la creazione del PDF dell'ordine tra i tuoi modelli ordine in Fattura24. 
                            Questo modello verrà usato se nell'ordine non è presente una destinazione.
                            Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                        </span>
                    </label>
                    <div class="col-lg-4">
                        <div class="fattura24-field">
                            <select name="PS_F24_MODELLO_ORDINE_" id="PS_F24_MODELLO_ORDINE_">
                                {html_options options=$listaModelliOrdine selected=$modelloOrdine}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Modello ordine accompagnatorio&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Seleziona il modello da usare per la creazione del PDF dell'ordine tra i tuoi modelli ordine in Fattura24. 
                            Questo modello verrà usato se nell'ordine è presente una destinazione.
                            Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                        </span>
                    </label>
                    <div class="col-lg-4">
                        <div class="fattura24-field">
                            <select name="PS_F24_MODELLO_ORDINE_DEST_" id="PS_F24_MODELLO_ORDINE_DEST_">
                                {html_options options=$listaModelliOrdine selected=$modelloOrdineDest}
                            </select>
                        </div>
                    </div>
                </div>
                <br/>
                
                <div class="form-group col-lg-12">
                    <h2>Fatture</h2>
                    <hr>
                </div>
            
                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Crea fattura&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi che venga creata la fattura tramite Fattura24 e che sia scaricata sul tuo Prestashop quando lo stato 
                            dell'ordine viene impostato come 'pagato">?</a>
                        </span>
                    </label>
                    <div class="col-lg-3">
                        <div class="fattura24-field">
                            <select name="PS_CREAZIONE_FATTURA_" id="PS_CREAZIONE_FATTURA_">
                                {html_options options=$opzioniCreaFattura selected=$creazioneFattura}
                            </select>
                        </div>
                    </div>
                </div>
                    
                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Causale Fattura&nbsp; 
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                                title="Inserisci (N) per riportare in causale il numero d'ordine, (R) per il riferimento Prestashop, o una combinazione di entrambi">?</a>
                        </span>
                    </label>
                    <div class="col-lg-3">
                        <div class="fattura24-field">
                            <input type="text"
                                name="PS_INV_OBJECT"
                                id="PS_INV_OBJECT"
                                value="{$invObject}"
                            />
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div>
                            <button type="button" class="btn btn-default" id="F24Object"> Predefinito </button>&nbsp; <!-- pulsante aggiornato in data 24.10.2019 -->
                        </div>
                    </div>
                </div>
                    
                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Natura IVA&nbsp; 
                            <span class="help">
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                                title="Inserisci">?</a>
                            </span>
                    </label>
                    <div class="col-lg-2">
                        <div class="fattura24-field">
                            <input type="text"
                                name="PS_CONF_NATURA_IVA"
                                id="PS_CONF_NATURA_IVA"
                                value="{$confNaturaIva}"
                            />
                        </div>
                    </div>
                </div>
                    
                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Natura IVA Spedizione&nbsp; 
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Scegli">?</a>
                        </span>
                    </label>
                    <div class="col-lg-2">
                        <div class="fattura24-field">
                            <select name="PS_CONF_NATURA_SPEDIZIONE" id="PS_CONF_NATURA_SPEDIZIONE">
                                {html_options options=$tipiAliquoteIva selected=$naturaSpedizione}
                            </select>
                        </div>
                    </div>
                </div>
                    
                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Invia email&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi che il PDF della fattura venga spedito automaticamente al cliente via email">?</a>
                        </span>
                    </label>
                    <div class="col-lg-1">
                        <div class="checkbox">
                            <label for="PS_EMAIL_FATTURA_">
                                <input type="checkbox" name="PS_EMAIL_FATTURA_"
                                    id="PS_EMAIL_FATTURA_"
                                    value="1"
                                    {if $emailFattura eq 1}
                                        checked="checked"
                                    {/if}
                                />
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Movimenta magazzino&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Spunta questo campo se vuoi attivare la movimentazione del magazzino tramite la creazione delle fatture. 
                            I prodotti in Fattura24 che hanno lo stesso codice dei prodotti di Prestashop saranno scaricati">?</a>
                        </span>
                    </label>
                    <div class="col-lg-1">
                        <div class="checkbox">
                            <label for="PS_F24_MAGAZZINO_FATTURA_">
                                <input type="checkbox" name="PS_F24_MAGAZZINO_FATTURA_"
                                    id="PS_F24_MAGAZZINO_FATTURA_"
                                    value="1"
                                    {if $magazzinoFattura eq 1}
                                            checked="checked"
                                    {/if}
                                />
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Stato 'Pagato'&nbsp;
                            <span class="help">
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                                    title="Scegli la condizione per creare il documento nello stato 'Pagato'">?</a>
                            </span>
                    </label>
                    <div class="col-lg-3">
                        <div class="fattura24-field">
                            <select name="PS_STATO_PAGATO_" id="PS_STATO_PAGATO_">
                                {html_options options=$opzioniStatoPagato selected=$statoPagato}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Disabilita creazione ricevute&nbsp;
                            <span class="help">
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                                title="Spunta questo campo se vuoi che la fattura sia creata anche in assenza della Partita IVA del cliente">?</a>
                            </span>
                    </label>
                    <div class="col-lg-1">
                        <div class="checkbox">
                            <label for="PS_DISABILITA_RICEVUTE_">
                                <input type="checkbox" name="PS_DISABILITA_RICEVUTE_"
                                    id="PS_DISABILITA_RICEVUTE_"
                                    value="1"
                                    {if $disabilitaRicevute eq 1}
                                            checked="checked"
                                    {/if}
                                />
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Modello fattura&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Seleziona il modello da usare per la creazione del PDF della fattura tra i tuoi modelli fattura in Fattura24. 
                            Questo modello verrà usato se nell'ordine non è presente una destinazione.
                            Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                        </span>
                    </label>
                    <div class="col-lg-4">
                        <div class="fattura24-field">
                            <select name="PS_F24_MODELLO_FATTURA_" id="PS_F24_MODELLO_FATTURA_">
                                {html_options options=$listaModelliFattura selected=$modelloFattura}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label  col-lg-2">
                        Modello fattura accompagnatoria&nbsp;
                            <span class="help">
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                                title="Seleziona il modello da usare per la creazione del PDF della fattura tra i tuoi modelli fattura in Fattura24. 
                                Questo modello verrà usato se nell'ordine è presente una destinazione.
                                Se è selezionato 'Predefinito', sarà usato il modello impostato come modello predefinito in Fattura24. 
                                Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                            </span>
                    </label>
                    <div class="col-lg-4">
                        <div class="fattura24-field">
                            <select name="PS_F24_MODELLO_FATTURA_DEST_" id="PS_F24_MODELLO_FATTURA_DEST_">
                                    {html_options options=$listaModelliFattura selected=$modelloFatturaDest}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Piano dei conti&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Puoi selezionare il conto da associare alle prestazioni/prodotti dei documenti tra i tuoi conti in Fattura24. 
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                        </span>
                    </label>
                    <div class="col-lg-4">
                        <div class="fattura24-field">
                            <select name="PS_F24_PDC_" id="PS_F24_PDC_">
                                {html_options options=$listaConti selected=$conto}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Sezionale ricevute&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Puoi selezionare il sezionale da usare per la numerazione delle ricevute tra i sezionali attivi per le ricevute in Fattura24.
                            Se è selezionato 'Predefinito', sarà usato il sezionale che hai impostato come predefinito in Fattura24.
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                        </span>
                    </label>
                    <div class="col-lg-2">
                        <div class="fattura24-field">
                            <select name="PS_F24_SEZIONALE_RICEVUTA_" id="PS_F24_SEZIONALE_RICEVUTA_">
                                {html_options options=$listaSezionaliRicevuta selected=$sezionaleRicevuta}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Sezionale fatture&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Seleziona il sezionale da usare per la numerazione delle fatture tra i sezionali attivi per le fatture in Fattura24.
                            Se è selezionato 'Predefinito', sarà usato il sezionale che hai impostato come predefinito in Fattura24.
                            Per visualizzare la lista, devi prima salvare la tua Api Key di Fattura24 in Prestashop">?</a>
                        </span>
                    </label>
                    <div class="col-lg-2">
                        <div class="fattura24-field">
                            <select name="PS_F24_SEZIONALE_FATTURA_" id="PS_F24_SEZIONALE_FATTURA_">
                                {if $creazioneFattura neq 1}
                                     {html_options options=$listaSezionaliFattura selected=$sezionaleFattura}
                                {elseif $creazioneFattura eq 1}
                                    {html_options options=$listaSezionaliFatturaElettronica selected=$sezionaleFattura}
                                {/if}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="fattura24-label col-lg-2">
                        Bollo virtuale fatture elettroniche&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Pagherò il bollo virtuale per conto del cliente">?</a>
                        </span>
                    </label>
                    <div class="col-lg-4">
                        <div class="fattura24-field">
                            <select name="PS_F24_BOLLO_VIRTUALE_FE" id="PS_F24_BOLLO_VIRTUALE_FE">
                                {html_options options=$bolloVirtualeFe selected=$bollo}
                            </select>
                        </div>
                    </div>
                </div>
                <br/>
                    
                <div class="form-group col-lg-12">
                    <h2>Log</h2>
                    <hr>
                </div>
                
                <div class="form-group">
                    <div class="col-lg-2">                        
                        <input type="button" class="btn btn-default" id="download_log_file" value="Scarica file di log"/>&nbsp;&nbsp;&nbsp;
                        <span class="help">
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                            title="Scarica e invia questo file a info@fattura24.com quando riscontri problemi con il modulo di Fattura24">?</a>
                        </span>
                    </div>
                </div>
        
                <div class="form-group">
                    <div class="col-lg-3">                     
                        <input type="button" class="btn btn-default" id="delete_log_file" value="Cancella file di log"/>&nbsp;
                            <span class="help">
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" 
                                title="Cancella il file di log dalla cartella del modulo di Fattura24. Ciò è utile per liberare spazio sul proprio server">?</a>
                            </span>
                    </div>
                </div>
                <br/>

                <div class="form-group">
                    <h2>DOCUMENTAZIONE</h2>
                    <hr>
                    <button type="submit" value="1" id="module_form_submit_btn_footer" name="submitModule"
                        class="btn btn-default pull-right">
                        <i class="process-icon-save"></i> Salva
                    </button>
                    <a class="btn btn-default" href="https://www.app.fattura24.com/v3/" target="_blank">Accedi a Fattura24 </a><br />
                    <br /><br />
                                    
                    <a href= "https://www.fattura24.com/prestashop-modulo-fatturazione/" target="_blank"> Supporto </a><br />
                    <a href= "https://www.fattura24.com/termini-utilizzo/" target="_blank"> Condizioni di contratto e termini di utilizzo di Fattura24</a><br />
                    <a href= "https://www.fattura24.com/regolamento-ecommerce/" target="_blank"> Regolamento F24 integrativo (delle Condizioni di Contratto) per modulo Prestashop</a><br />
                    <a href= "https://www.fattura24.com/policy/" target="_blank"> Privacy di Fattura24</a><br />
                    <a style ="color: darkred;" href= "https://www.fattura24.com/circolare-14e-ade-del-17-giugno-2019/" target="_blank"> Circolare Agenzia Entrate sul campo della Data-Fattura</a><br />
                </div>
            </div>
        </div><!-- /.form-wrapper -->
    </div>
</form>

<script type="text/javascript">

    function downloadFile(dataurl, filename)
    {
        var a = document.createElement("a");
        a.href = dataurl;
        a.setAttribute("download", filename);
        var b = document.createEvent("MouseEvents");
        b.initEvent("click", false, true);
        a.dispatchEvent(b);
        return false;
    }

    document.addEventListener("DOMContentLoaded", function (e)
    {
        var downloadLogFile = document.getElementById("download_log_file");
        downloadLogFile.addEventListener('click', function (e)
        {
            $.ajax({
                method: 'POST',
                data: 'info={$info}',
                url: '{$moduleUrl}' + 'log/downloadLogFile.php',
                dataType: 'json',
                error: function (response)
                {
                    alert('Errore durante il download.');
                },
                complete: function()
                {
					var today = new Date();
					var dateformat = today.getDate() + '-' + (today.getMonth()+1) + '-' + today.getFullYear() + '_' + today.getHours() + '_' + today.getMinutes();
					var new_filename = 'f24_trace_' + dateformat + '.log';
                    downloadFile('{$moduleUrl}' + 'log/trace.log', new_filename); // rinomino il file al Download
                }
            });
        });

        var deleteLogFile = document.getElementById("delete_log_file");
        deleteLogFile.addEventListener('click', function (e)
        {
            var r = confirm("Sei sicuro di voler cancellare il file di log di Fattura24?");
            if (r == true)
            {
                $.ajax({
                    url: '{$moduleUrl}' + 'log/deleteLogFile.php',
                    dataType: 'json',
                    success: function (response)
                    {
                        if(response.fileExists === true)
                        {
                            alert('File cancellato! La sua dimensione era: ' + response.size + ' KB.');
                        }
                        else if(response.fileExists === false)
                        {
                            alert('Il file è già stato cancellato.');
                        }
                    }
                });
            }
        });

        // Rubrica
        var PS_SALVA_CLIENTE = document.getElementById('PS_SALVA_CLIENTE_');

        // Ordini
        var PS_CREAZIONE_ORDINE = document.getElementById('PS_CREAZIONE_ORDINE_');
        var PS_PDF_ORDINE = document.getElementById('PS_PDF_ORDINE_');
        var PS_MAIL_ORDINE = document.getElementById('PS_MAIL_ORDINE_');
        var PS_F24_MAGAZZINO_ORDINE = document.getElementById('PS_F24_MAGAZZINO_ORDINE_');
        var PS_F24_MODELLO_ORDINE = document.getElementById('PS_F24_MODELLO_ORDINE_');
        var PS_F24_MODELLO_ORDINE_DEST = document.getElementById('PS_F24_MODELLO_ORDINE_DEST_');

        if (!PS_CREAZIONE_ORDINE.checked){
            toggle(PS_PDF_ORDINE, 'disabled');
            toggle(PS_MAIL_ORDINE, 'disabled');
            toggle(PS_F24_MAGAZZINO_ORDINE, 'disabled');
            toggle(PS_F24_MODELLO_ORDINE, 'disabled');
            toggle(PS_F24_MODELLO_ORDINE_DEST, 'disabled');
        }
        if (PS_CREAZIONE_ORDINE.checked){
            PS_SALVA_CLIENTE.checked = true;
            PS_SALVA_CLIENTE.disabled = true;
        }

        // Listener per creazione ordine
        PS_CREAZIONE_ORDINE.addEventListener('change', function (e){
            if (PS_CREAZIONE_ORDINE.checked){
                toggle(PS_PDF_ORDINE);
                toggle(PS_MAIL_ORDINE);
                toggle(PS_F24_MAGAZZINO_ORDINE);
                toggle(PS_F24_MODELLO_ORDINE);
                toggle(PS_F24_MODELLO_ORDINE_DEST);
                PS_SALVA_CLIENTE.checked = true;
                PS_SALVA_CLIENTE.disabled = true;
            }
            else{
                toggle(PS_PDF_ORDINE, 'disabled');
                toggle(PS_MAIL_ORDINE, 'disabled');
                toggle(PS_F24_MAGAZZINO_ORDINE, 'disabled');
                toggle(PS_F24_MODELLO_ORDINE, 'disabled');
                toggle(PS_F24_MODELLO_ORDINE_DEST, 'disabled');
                if(!PS_CREAZIONE_FATTURA.checked)
                    PS_SALVA_CLIENTE.disabled = false;
            }
        });

        var PS_CREAZIONE_FATTURA = document.getElementById('PS_CREAZIONE_FATTURA_');
        var valueSelected = PS_CREAZIONE_FATTURA.options[PS_CREAZIONE_FATTURA.selectedIndex].text;
        var PS_DISABILITA_RICEVUTE = document.getElementById('PS_DISABILITA_RICEVUTE_');
        var PS_EMAIL_FATTURA = document.getElementById('PS_EMAIL_FATTURA_');
        var PS_STATO_PAGATO = document.getElementById('PS_STATO_PAGATO_');

        var PS_INV_OBJECT = document.getElementById('PS_INV_OBJECT');       
        var PS_CONF_NATURA_IVA = document.getElementById('PS_CONF_NATURA_IVA');
        var PS_CONF_NATURA_SPEDIZIONE = document.getElementById('PS_CONF_NATURA_SPEDIZIONE');
        var PS_F24_MAGAZZINO_FATTURA = document.getElementById('PS_F24_MAGAZZINO_FATTURA_');
        var PS_F24_MODELLO_FATTURA = document.getElementById('PS_F24_MODELLO_FATTURA_');
        
        var PS_F24_MODELLO_FATTURA_DEST = document.getElementById('PS_F24_MODELLO_FATTURA_DEST_');
        var PS_F24_PDC = document.getElementById('PS_F24_PDC_');
        
        var PS_F24_SEZIONALE_RICEVUTA = document.getElementById('PS_F24_SEZIONALE_RICEVUTA_');
        var PS_F24_SEZIONALE_FATTURA = document.getElementById('PS_F24_SEZIONALE_FATTURA_');
        
        if (valueSelected === "Disattivata"){
            toggle(PS_DISABILITA_RICEVUTE, 'disabled');
            toggle(PS_EMAIL_FATTURA, 'disabled');
            toggle(PS_STATO_PAGATO, 'disabled');
            toggle(PS_F24_MAGAZZINO_FATTURA, 'disabled');
            toggle(PS_F24_MODELLO_FATTURA, 'disabled');
            toggle(PS_F24_MODELLO_FATTURA_DEST, 'disabled');
            toggle(PS_F24_PDC, 'disabled');
            toggle(PS_F24_SEZIONALE_RICEVUTA, 'disabled');
            toggle(PS_F24_SEZIONALE_FATTURA, 'disabled');
            toggle(PS_INV_OBJECT, 'disabled');
            toggle(PS_CONF_NATURA_IVA, 'readonly');
            toggle(PS_CONF_NATURA_SPEDIZIONE, 'readonly');
        }

        if (valueSelected === "Fattura NON elettronica"){
            PS_SALVA_CLIENTE.checked = true;
            PS_SALVA_CLIENTE.disabled = true;
            toggle(PS_CONF_NATURA_IVA, 'readonly');
            toggle(PS_CONF_NATURA_SPEDIZIONE, 'readonly');
        
        } else if (valueSelected === "Fattura elettronica"){
            toggle(PS_CONF_NATURA_IVA, 'enabled');
            toggle(PS_CONF_NATURA_SPEDIZIONE, 'enabled');
            toggle(PS_EMAIL_FATTURA, 'disabled');
        }
        
        
        // Listener per creazione fatture
        PS_CREAZIONE_FATTURA.addEventListener('change', function (e){
            var valueSelected = PS_CREAZIONE_FATTURA.options[PS_CREAZIONE_FATTURA.selectedIndex].text;
            console.log("SELECTED OPTION FATTURA = "+valueSelected);
            if (valueSelected === "Fattura NON elettronica"){
                toggle(PS_EMAIL_FATTURA);
                toggle(PS_STATO_PAGATO);
                toggle(PS_DISABILITA_RICEVUTE);
                toggle(PS_F24_MAGAZZINO_FATTURA);
                toggle(PS_F24_MODELLO_FATTURA);
                toggle(PS_F24_MODELLO_FATTURA_DEST);
                toggle(PS_F24_PDC);
                toggle(PS_F24_SEZIONALE_RICEVUTA);
                toggle(PS_F24_SEZIONALE_FATTURA);
                toggle(PS_CONF_NATURA_IVA, 'readonly');
                toggle(PS_CONF_NATURA_SPEDIZIONE, 'readonly');
                
                PS_SALVA_CLIENTE.checked = true;
                PS_SALVA_CLIENTE.disabled = true;

            } else if(valueSelected === "Disattivata"){
                toggle(PS_INV_OBJECT, 'disabled');
                toggle(PS_EMAIL_FATTURA, 'disabled');
                toggle(PS_STATO_PAGATO, 'disabled');
                toggle(PS_DISABILITA_RICEVUTE, 'disabled');
                toggle(PS_F24_MAGAZZINO_FATTURA, 'disabled');
                toggle(PS_F24_MODELLO_FATTURA, 'disabled');
                toggle(PS_F24_MODELLO_FATTURA_DEST, 'disabled');
                toggle(PS_F24_PDC, 'disabled');
                toggle(PS_F24_SEZIONALE_RICEVUTA, 'disabled');
                toggle(PS_F24_SEZIONALE_FATTURA, 'disabled');
                toggle(PS_CONF_NATURA_IVA, 'readonly');
                toggle(PS_CONF_NATURA_SPEDIZIONE, 'readonly');
                if(!PS_CREAZIONE_ORDINE.checked)
                    PS_SALVA_CLIENTE.disabled = false;
            } else {
                    toggle(PS_EMAIL_FATTURA, 'disabled');
                    toggle(PS_CONF_NATURA_IVA);
                    toggle(PS_CONF_NATURA_SPEDIZIONE);
            }
                   
           
        });
        /**
        * cambia la proprietà di un elemento utilizzando il parametro status
        * Nota: l'attributo 'readonly' rende il campo non modificabile
        */
        function toggle(element, status){
            if(status === 'disabled'){
                $(element).prop('checked', false)
                $(element).prop('disabled',true)
            } else if(status === 'readonly'){
                $(element).prop('disabled', true)
            } else {
                $(element).prop('disabled', false)
        }

      
        //GESTIONE DEI TOOLTIP
        $('[data-toggle="tooltip"]').tooltip();

        /*var show_hide = document.getElementById("show_hide");
        show_hide.addEventListener('click', function(e) {
            let input = $('input#PS_FATTURA24_API').attr('type');
            if (input === 'password') {
                $('input#PS_FATTURA24_API').attr('type', 'text');
                $('span#show_hide').attr('class', 'icon-lock field-icon');
            }else {
                $('input#PS_FATTURA24_API').attr('type', 'password');
                $('span#show_hide').attr('class', 'icon-unlock field-icon');
            }
        });*/
     
       /*
       * Pulsante per il valore predefinito del campo oggetto:
       *  Premendolo posso decidere in ogni momento di utilizzare come causale 
       *  il testo predefinito - Davide Iandoli 29.07.2019 -- Aggiornato in data 24.10.2019
       */
       var Objectbtn = document.getElementById("F24Object");

       Objectbtn.addEventListener('click', function (e) {
          document.getElementById("PS_INV_OBJECT").defaultValue = "Ordine (N) Shop on-line";
       });

        // aggiungo il bottone per verifica connessione
        var panels = document.getElementsByClassName('panel');
        var panel = panels[0];

        var btn = document.getElementById("verify");

        btn.addEventListener('click', function (e) {
            var apiKey = PS_FATTURA24_API.value;
            var xhr = new XMLHttpRequest();
            var params = "apiKey=" + apiKey;
            xhr.open("post", "https://www.app.fattura24.com/api/v0.3/TestKey", true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
            xhr.send(params);
            xhr.onload = function () {
                console.log(this.responseText);
                var response = this.responseText.replace("&egrave;", ""); //devo togliere questa parte altrimenti da problemi con l'encoding xml
                console.log(response);
                var parser = new DOMParser();
                var xml = parser.parseFromString(response, "text/xml");
                var code = xml.getElementsByTagName('returnCode');
  
                var description = xml.getElementsByTagName('description');
                var type = xml.getElementsByTagName('type'); // tipo di abbonamento
                var expire = xml.getElementsByTagName('expire'); // data di scadenza
                
                if (code[0].childNodes[0].nodeValue == 1) {
                    try {  
                        document.getElementById("result").innerHTML = "<span style='color: green; font-size: 120%;'>API Key verificata! </span><br/>"; 
                        document.getElementById("expire").innerHTML = "<span style='color: green; font-size: 120%;'>" + expire[0].childNodes[0].nodeValue + "</span>";
                    } catch (e) {
                        document.getElementById("result").innerHTML = "<span style='color: red; font-size: 120%;'>Account di Test! </span></br>";
                    }                 

                } else if(!apiKey) {
                     document.getElementById("result").innerHTML = "<span style='color: red; font-size: 120%;'>API Key non inserita! </span></br>"; 
                               
				} else {	
                    try {
							if (type[0].childNodes[0].nodeValue < 5) { 
								document.getElementById("expire").innerHTML = "<span style='color: red; font-size: 120%;'>Per usare il servizio è necessario un abbonamento Business o Enterprise! </span></br>";
							}
					} catch (e) {
						document.getElementById("result").innerHTML = "<span style='color: red; font-size: 120%;'>API Key non valida! </span></br>";
    				}	
                }
            }
        }); // end of button event listener
      }   
   }); // end of DOM function

</script>