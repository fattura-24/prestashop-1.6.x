<?php
/**
 * Questo file è parte del plugin Prestashop v1.6.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Descrizione: gestione impostazioni e azioni del modulo
 * usa templates e hooks per modificare e gestire dati dell'ordine e dell'utente 
 */

class Fattura24 extends Module{
 
    public function __construct(){
       $this->baseUrl = "https://www.app.fattura24.com";
       $this->name = 'fattura24';
       $this->tab = 'administration';
       $this->version = '1.7.11';
       $this->moduleDate = '2020/07/06 15:00';
       $this->author = 'Fattura24.com';
       $this->displayName = $this->l('Fattura24.com');
       $this->description = $this->l('Crea le tue fatture con Fattura24.com');
       $this->module_key = '00cd0339f7de777834493205747ac875';
       $this->need_instance = 0;
       $this->bootstrap = true;
       parent::__construct();
    }

    public function install(){
        // set default configuration values on first installation
        if(strlen(Configuration::get('PS_FATTURA24_API')) == 0)
            Configuration::updateValue('PS_FATTURA24_API', '');
        if(strlen(Configuration::get('PS_SALVA_CLIENTE')) == 0)
            Configuration::updateValue('PS_SALVA_CLIENTE', '1');
        if(strlen(Configuration::get('PS_CREAZIONE_ORDINE')) == 0)
            Configuration::updateValue('PS_CREAZIONE_ORDINE', '1');
        if(strlen(Configuration::get('PS_PDF_ORDINE')) == 0)
            Configuration::updateValue('PS_PDF_ORDINE', '0');
        if(strlen(Configuration::get('PS_MAIL_ORDINE')) == 0)
            Configuration::updateValue('PS_MAIL_ORDINE', '0');
        if(strlen(Configuration::get('PS_F24_MAGAZZINO_ORDINE')) == 0)
            Configuration::updateValue('PS_F24_MAGAZZINO_ORDINE', '0');
        if(strlen(Configuration::get('PS_F24_MODELLO_ORDINE')) == 0)
            Configuration::updateValue('PS_F24_MODELLO_ORDINE', 'Predefinito');
        if(strlen(Configuration::get('PS_F24_MODELLO_ORDINE_DEST')) == 0)
            Configuration::updateValue('PS_F24_MODELLO_ORDINE_DEST', 'Predefinito');
        if(strlen(Configuration::get('PS_CREAZIONE_FATTURA')) == 0)
            Configuration::updateValue('PS_CREAZIONE_FATTURA', 'Disattivata');
        if(strlen(Configuration::get('PS_CONF_NATURA_IVA')) == 0)
            Configuration::updateValue('PS_CONF_NATURA_IVA', '');
            if(strlen(Configuration::get('PS_CONF_NATURA_SPEDIZIONE')) == 'N0')
            Configuration::updateValue('PS_CONF_NATURA_SPEDIZIONE', 'Nessuna'); // nuova impostazione natura Iva spedizione
        if(strlen(Configuration::get('PS_INV_OBJECT')) == 0) // nuovo campo per l'oggetto personalizzato
            Configuration::updateValue('PS_INV_OBJECT', '');
        if(strlen(Configuration::get('PS_EMAIL_FATTURA')) == 0)
            Configuration::updateValue('PS_EMAIL_FATTURA', '0');
        if(strlen(Configuration::get('PS_F24_MAGAZZINO_FATTURA')) == 0)
            Configuration::updateValue('PS_F24_MAGAZZINO_FATTURA', '0');
        if(strlen(Configuration::get('PS_STATO_PAGATO')) == 0)
            Configuration::updateValue('PS_STATO_PAGATO', '0');
        if(strlen(Configuration::get('PS_DISABILITA_RICEVUTE')) == 0)
            Configuration::updateValue('PS_DISABILITA_RICEVUTE', '0');
        if(strlen(Configuration::get('PS_F24_MODELLO_FATTURA')) == 0)
            Configuration::updateValue('PS_F24_MODELLO_FATTURA', 'Predefinito');
        if(strlen(Configuration::get('PS_F24_MODELLO_FATTURA_DEST')) == 0)
            Configuration::updateValue('PS_F24_MODELLO_FATTURA_DEST', 'Predefinito');
        if(strlen(Configuration::get('PS_F24_PDC')) == 0)
            Configuration::updateValue('PS_F24_PDC', 'Nessun Pdc');
        if(strlen(Configuration::get('PS_F24_SEZIONALE_RICEVUTA')) == 0)
            Configuration::updateValue('PS_F24_SEZIONALE_RICEVUTA', 'Predefinito');
        if(strlen(Configuration::get('PS_F24_SEZIONALE_FATTURA')) == 0)
            Configuration::updateValue('PS_F24_SEZIONALE_FATTURA', 'Predefinito');
        if(strlen(Configuration::get('PS_F24_BOLLO_VIRTUALE_FE')) == 0)
            Configuration::updateValue('PS_F24_BOLLO_VIRTUALE_FE', 'Mai');    
        
        $dim = "UPDATE `" . _DB_PREFIX_ . "order_state` SET `pdf_invoice` = '0' WHERE `id_order_state` = '2'";
        Db::getInstance()->execute($dim);

        /**
        * Gestione campi SDI e PEC con tabella a parte
        */
        
        $sql = array();

        $sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'fattura24` (
            `id_fattura24` int(11) NOT NULL AUTO_INCREMENT,
            `id_customer` int(11),
            `fattura24_codice_destinatario` varchar(13),
            `fattura24_pec` varchar(60),
            PRIMARY KEY  (`id_fattura24`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

        foreach ($sql as $query) {
            if (Db::getInstance()->execute($query) == false) {
                return false;
            }
        }
        
        // aggiunge le colonne di fattura24 alla tabella ordini di PS (docIdFattura24 => Fatture)
        try{
            $sql = 'SELECT docIdFattura24, docIdOrderFattura24, apiResponseOrder, apiResponseInvoice from '._DB_PREFIX_.'orders';
            $result = Db::getInstance()->ExecuteS($sql);
            if(!$result){
                Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD docIdFattura24 varchar(255) DEFAULT NULL');
                Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD docIdOrderFattura24 varchar(255) DEFAULT NULL');
                Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD apiResponseOrder JSON DEFAULT NULL');
                Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD apiResponseInvoice JSON DEFAULT NULL');
            }    
        }
        catch(Exception $e){
            Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD docIdFattura24 varchar(255) DEFAULT NULL');
            Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD docIdOrderFattura24 varchar(255) DEFAULT NULL');
            Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD apiResponseOrder varchar(255) DEFAULT NULL');
            Db::getInstance()->execute('ALTER TABLE '._DB_PREFIX_.'orders ADD apiResponseInvoice varchar(255) DEFAULT NULL');
        }
                
        // registro gli hooks che utilizzo
        if (!parent::install()
            || !$this->registerHook('displayAdminCustomers')
            || !$this->registerHook('displayAdminOrder')
            || !$this->registerHook('displayCustomerAccountForm')
            || !$this->registerHook('displayCustomerIdentityForm')
            || !$this->registerHook('actionCustomerAccountAdd')
            || !$this->registerHook('actionCustomerAccountUpdate')
            || !$this->registerHook('actionValidateOrder')
            || !$this->registerHook('actionOrderStatusUpdate')
            || !$this->registerHook('actionOrderStatusPostUpdate'))
        return false;

         // crea la cartella per i PDF
        $documentsFolder = _PS_DOWNLOAD_DIR_ . 'Fattura24/';
        if (!file_exists($documentsFolder))
            mkdir($documentsFolder , 0777, true);
        if (!file_exists($documentsFolder . 'index.php'))
            file_put_contents($documentsFolder . 'index.php', '<?php');
        
        $this->trace('Modulo installato correttamente!');
        return true;
    }

    public function getContent(){

        $f24_template_url = $this->baseUrl.'/api/v0.3/GetTemplate';
        $f24_pdc_url = $this->baseUrl.'/api/v0.3/GetPdc';
        $f24_sezionali_url = $this->baseUrl.'/api/v0.3/GetNumerator';
        $send_data = array();
        $send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $templates = $this->curlDownload($f24_template_url, http_build_query($send_data));
        $f24_pdc = $this->curlDownload($f24_pdc_url, http_build_query($send_data));
        $f24_sezionali = $this->curlDownload($f24_sezionali_url, http_build_query($send_data));
        
        // qui gestisco i messaggi di errore quando entro nelle impostazioni
        $output = $this->getVersionCheckMessage() . $this->getNaturaError();


        if (Tools::isSubmit('submitModule')){
            Configuration::updateValue('PS_FATTURA24_API', $_REQUEST['PS_FATTURA24_API']);
            if(isset($_REQUEST['PS_CONF_NATURA_IVA']))
                Configuration::updateValue('PS_CONF_NATURA_IVA', $_REQUEST['PS_CONF_NATURA_IVA']);
            if(isset($_REQUEST['PS_CONF_NATURA_SPEDIZIONE']))    
                Configuration::updateValue('PS_CONF_NATURA_SPEDIZIONE', $_REQUEST['PS_CONF_NATURA_SPEDIZIONE']); // Natura  aliquota spedizione
            if(isset($_REQUEST['PS_INV_OBJECT']))
                Configuration::updateValue('PS_INV_OBJECT', $_REQUEST['PS_INV_OBJECT']); // oggetto della fattura personalizzato
            
            //se le checkbox sono settate imposto il valore della relativa configurazione a 1, altrimenti a 0
            if (isset($_REQUEST['PS_SALVA_CLIENTE_']))
            	Configuration::updateValue('PS_SALVA_CLIENTE', $_REQUEST['PS_SALVA_CLIENTE_']);
            else
                Configuration::updateValue('PS_SALVA_CLIENTE', '0');
            
            if (isset($_REQUEST['PS_CREAZIONE_ORDINE_']))
                Configuration::updateValue('PS_CREAZIONE_ORDINE', $_REQUEST['PS_CREAZIONE_ORDINE_']);
            else
                Configuration::updateValue('PS_CREAZIONE_ORDINE', '0');

            if (isset($_REQUEST['PS_PDF_ORDINE_']))
                Configuration::updateValue('PS_PDF_ORDINE', $_REQUEST['PS_PDF_ORDINE_']);
            else
                Configuration::updateValue('PS_PDF_ORDINE', '0');

            if (isset($_REQUEST['PS_MAIL_ORDINE_']))
                Configuration::updateValue('PS_MAIL_ORDINE', $_REQUEST['PS_MAIL_ORDINE_']);
            else
                Configuration::updateValue('PS_MAIL_ORDINE', '0');

            if (isset($_REQUEST['PS_F24_MODELLO_ORDINE_']))
                Configuration::updateValue('PS_F24_MODELLO_ORDINE', $_REQUEST['PS_F24_MODELLO_ORDINE_']);
            else
                Configuration::updateValue('PS_F24_MODELLO_ORDINE', 'Predefinito');

            if (isset($_REQUEST['PS_F24_MODELLO_ORDINE_DEST_']))
                Configuration::updateValue('PS_F24_MODELLO_ORDINE_DEST', $_REQUEST['PS_F24_MODELLO_ORDINE_DEST_']);
            else
                Configuration::updateValue('PS_F24_MODELLO_ORDINE_DEST', 'Predefinito');

            if (isset($_REQUEST['PS_CREAZIONE_FATTURA_']))
                Configuration::updateValue('PS_CREAZIONE_FATTURA', $_REQUEST['PS_CREAZIONE_FATTURA_']);
            else
                Configuration::updateValue('PS_CREAZIONE_FATTURA', '0');

            if (isset($_REQUEST['PS_EMAIL_FATTURA_']))
                Configuration::updateValue('PS_EMAIL_FATTURA', $_REQUEST['PS_EMAIL_FATTURA_']);
            else
                Configuration::updateValue('PS_EMAIL_FATTURA', '0');

            if (isset($_REQUEST['PS_STATO_PAGATO_']))
                Configuration::updateValue('PS_STATO_PAGATO', $_REQUEST['PS_STATO_PAGATO_']);
            else
                Configuration::updateValue('PS_STATO_PAGATO', '0');
            
            if (isset($_REQUEST['PS_DISABILITA_RICEVUTE_']))
            	Configuration::updateValue('PS_DISABILITA_RICEVUTE', $_REQUEST['PS_DISABILITA_RICEVUTE_']);
            else
            	Configuration::updateValue('PS_DISABILITA_RICEVUTE', '0');

            if (isset($_REQUEST['PS_F24_MAGAZZINO_ORDINE_']))
            	Configuration::updateValue('PS_F24_MAGAZZINO_ORDINE', $_REQUEST['PS_F24_MAGAZZINO_ORDINE_']);
            else
            	Configuration::updateValue('PS_F24_MAGAZZINO_ORDINE', '0');

            if (isset($_REQUEST['PS_F24_MAGAZZINO_FATTURA_']))
            	Configuration::updateValue('PS_F24_MAGAZZINO_FATTURA', $_REQUEST['PS_F24_MAGAZZINO_FATTURA_']);
            else
                Configuration::updateValue('PS_F24_MAGAZZINO_FATTURA', '0');
            
            if (isset($_REQUEST['PS_F24_MODELLO_FATTURA_']))
                Configuration::updateValue('PS_F24_MODELLO_FATTURA', $_REQUEST['PS_F24_MODELLO_FATTURA_']);
            else
                Configuration::updateValue('PS_F24_MODELLO_FATTURA', 'Predefinito');
            
            if (isset($_REQUEST['PS_F24_MODELLO_FATTURA_DEST_']))
                Configuration::updateValue('PS_F24_MODELLO_FATTURA_DEST', $_REQUEST['PS_F24_MODELLO_FATTURA_DEST_']);
            else
                Configuration::updateValue('PS_F24_MODELLO_FATTURA_DEST', 'Predefinito');
            
            if (isset($_REQUEST['PS_F24_PDC_']))
                Configuration::updateValue('PS_F24_PDC', $_REQUEST['PS_F24_PDC_']);
            else
                Configuration::updateValue('PS_F24_PDC', 'Nessun Pdc');

            if (isset($_REQUEST['PS_F24_SEZIONALE_RICEVUTA_']))
                Configuration::updateValue('PS_F24_SEZIONALE_RICEVUTA', $_REQUEST['PS_F24_SEZIONALE_RICEVUTA_']);
            else
                Configuration::updateValue('PS_F24_SEZIONALE_RICEVUTA', 'Predefinito');

            if (isset($_REQUEST['PS_F24_SEZIONALE_FATTURA_']))
                Configuration::updateValue('PS_F24_SEZIONALE_FATTURA', $_REQUEST['PS_F24_SEZIONALE_FATTURA_']);
            else
                Configuration::updateValue('PS_F24_SEZIONALE_FATTURA', 'Predefinito');
            
            if (isset($_REQUEST['PS_F24_BOLLO_VIRTUALE_FE']))
                Configuration::updateValue('PS_F24_BOLLO_VIRTUALE_FE', $_REQUEST['PS_F24_BOLLO_VIRTUALE_FE']);
            else
                Configuration::updateValue('PS_F24_BOLLO_VIRTUALE_FE', 'Mai');        

            $output = $this->getVersionCheckMessage() . $this->getNaturaError(); // aggiorno i messaggi dopo il submit
            $output .= $this->displayConfirmation($this->l('Impostazioni aggiornate'));
        }
        
        // i valori delle impostazioni vengono assegnati a smarty, che li userà per i template (file *.tpl) 
        $this->context->smarty->assign(
            array(
                'isFattura24Enabled' => Module::isEnabled('fattura24'),
                'psDisableOverrides' => Configuration::get('PS_DISABLE_OVERRIDES'),
                'version' => $this->version,
                'apiKey' => Configuration::get('PS_FATTURA24_API'),
                'listaModelliOrdine' => $this->getTemplate($templates, true),
                'modelloOrdine' => Configuration::get('PS_F24_MODELLO_ORDINE'),
                'modelloOrdineDest' => Configuration::get('PS_F24_MODELLO_ORDINE_DEST'),
                'listaModelliFattura' => $this->getTemplate($templates, false),
                'opzioniCreaFattura' => ['Disattivata', 'Fattura elettronica', 'Fattura NON elettronica', 'Ricevuta NON elettronica'],
                //'tipiAliquoteIva' => $this->getListaNature_new(),
                'tipiAliquoteIva' => $this->getListaNature(), // natura IVA
                'opzioniStatoPagato' => ['Mai', 'Sempre', 'Pagamenti elettronici (es.: Paypal)'],
                'modelloFattura' => Configuration::get('PS_F24_MODELLO_FATTURA'),
                'modelloFatturaDest' => Configuration::get('PS_F24_MODELLO_FATTURA_DEST'),
                'listaConti' => $this->getPdc($f24_pdc),
                'conto' => Configuration::get('PS_F24_PDC'),
                'salvaCliente' => Configuration::get('PS_SALVA_CLIENTE'),
                'creazioneOrdine' => Configuration::get('PS_CREAZIONE_ORDINE'),
                'pdfOrdine' => Configuration::get('PS_PDF_ORDINE'),
                'emailOrdine' => Configuration::get('PS_MAIL_ORDINE'),
                'creazioneFattura' => Configuration::get('PS_CREAZIONE_FATTURA'),
                'confNaturaIva' => Configuration::get('PS_CONF_NATURA_IVA'),
                'naturaSpedizione' => Configuration::get('PS_CONF_NATURA_SPEDIZIONE'),
                'invObject' => Configuration::get('PS_INV_OBJECT'), // oggetto personalizzato
                'emailFattura' => Configuration::get('PS_EMAIL_FATTURA'),
                'statoPagato' => Configuration::get('PS_STATO_PAGATO'),
                'disabilitaRicevute' => Configuration::get('PS_DISABILITA_RICEVUTE'),
                'magazzinoOrdine' => Configuration::get('PS_F24_MAGAZZINO_ORDINE'),
                'magazzinoFattura' => Configuration::get('PS_F24_MAGAZZINO_FATTURA'),
                'listaSezionaliFattura' => $this->GetNumerator($f24_sezionali, 1),
                'listaSezionaliFatturaElettronica' => $this->GetNumerator($f24_sezionali, 11),
                'listaSezionaliRicevuta' => $this->GetNumerator($f24_sezionali, 3),
                'sezionaleRicevuta' => Configuration::get('PS_F24_SEZIONALE_RICEVUTA'),
                'sezionaleFattura' => Configuration::get('PS_F24_SEZIONALE_FATTURA'),
                'bolloVirtualeFe' => ['Mai', 'Regimi esenti IVA e totale ordine > 77.47 Euro'],
                'bollo' => Configuration::get('PS_F24_BOLLO_VIRTUALE_FE'),
                'moduleUrl' => _MODULE_DIR_ . 'fattura24/',
                'info' => $this->getInfo()
            )
        );

        $output .= $this->display(__FILE__, 'views/templates/admin/form.tpl');
        return $output;
    }
    // valori immessi nella schermata impostazioni
    public function getConfigFieldsValues(){
        return array(
            'PS_FATTURA24_API' => Tools::getValue('PS_FATTURA24_API', Configuration::get('PS_FATTURA24_API')),
        	'PS_SALVA_CLIENTE' => Tools::getValue('PS_SALVA_CLIENTE_', Configuration::get('PS_SALVA_CLIENTE')),
            'PS_CREAZIONE_ORDINE_' => Tools::getValue('PS_CREAZIONE_ORDINE_', Configuration::get('PS_CREAZIONE_ORDINE')),
            'PS_PDF_ORDINE_' => Tools::getValue('PS_PDF_ORDINE_', Configuration::get('PS_PDF_ORDINE')),
            'PS_MAIL_ORDINE_' => Tools::getValue('PS_MAIL_ORDINE_', Configuration::get('PS_MAIL_ORDINE')),
            'PS_F24_MODELLO_ORDINE_' => Tools::getValue('PS_F24_MODELLO_ORDINE_', Configuration::get('PS_F24_MODELLO_ORDINE')),
            'PS_F24_MODELLO_ORDINE_DEST_' => Tools::getValue('PS_F24_MODELLO_ORDINE_DEST_', Configuration::get('PS_F24_MODELLO_ORDINE_DEST')),
            'PS_CREAZIONE_FATTURA_' => Tools::getValue('PS_CREAZIONE_FATTURA_', Configuration::get('PS_CREAZIONE_FATTURA')),
            'PS_CONF_NATURA_IVA' => Tools::getValue('PS_CONF_NATURA_IVA', Configuration::get('PS_CONF_NATURA_IVA')),
            'PS_CONF_NATURA_SPEDIZIONE' => Tools::getValue('PS_CONF_NATURA_SPEDIZIONE', Configuration::get('PS_CONF_NATURA_SPEDIZIONE')),
            'PS_INV_OBJECT' => Tools::getValue('PS_INV_OBJECT', Configuration::get('PS_INV_OBJECT')), // oggetto personalizzato
            'PS_EMAIL_FATTURA_' => Tools::getValue('PS_EMAIL_FATTURA_', Configuration::get('PS_EMAIL_FATTURA')),
            'PS_STATO_PAGATO_' => Tools::getValue('PS_STATO_PAGATO_', Configuration::get('PS_STATO_PAGATO')),
            'PS_DISABILITA_RICEVUTE_' => Tools::getValue('PS_DISABILITA_RICEVUTE_', Configuration::get('PS_DISABILITA_RICEVUTE')),
            'PS_F24_MAGAZZINO_ORDINE_' => Tools::getValue('PS_F24_MAGAZZINO_ORDINE_', Configuration::get('PS_F24_MAGAZZINO_ORDINE')),
            'PS_F24_MAGAZZINO_FATTURA_' => Tools::getValue('PS_F24_MAGAZZINO_FATTURA_', Configuration::get('PS_F24_MAGAZZINO_FATTURA')),
            'PS_F24_MODELLO_FATTURA_' => Tools::getValue('PS_F24_MODELLO_FATTURA_', Configuration::get('PS_F24_MODELLO_FATTURA')),
            'PS_F24_MODELLO_FATTURA_DEST_' => Tools::getValue('PS_F24_MODELLO_FATTURA_DEST_', Configuration::get('PS_F24_MODELLO_FATTURA_DEST')),
            'PS_F24_PDC_' => Tools::getValue('PS_F24_PDC_', Configuration::get('PS_F24_PDC')),
            'PS_F24_SEZIONALE_FATTURA_' => Tools::getValue('PS_F24_SEZIONALE_FATTURA_', Configuration::get('PS_F24_SEZIONALE_FATTURA')),
            'PS_F24_SEZIONALE_RICEVUTA_' => Tools::getValue('PS_F24_SEZIONALE_RICEVUTA_', Configuration::get('PS_F24_SEZIONALE_RICEVUTA'))            
        );
    }
    
    // Hook per la creazione dell'ordine. Si attiva quando viene creato l'ordine
    public function hookActionValidateOrder($order){
        if(!$this->active)
            return;
        // fix per compatibilità con Prestashop 1.7
        if(isset($order['order']))
            $check_order = $order['order'];
        else
            $check_order = $order['objOrder'];
    	if (Configuration::get('PS_CREAZIONE_ORDINE') == 1)
            $this->afterHook($check_order, false);    
        else if(Configuration::get('PS_SALVA_CLIENTE') == 1)
    		$this->saveCustomer($check_order);
    }

    // Hook per la creazione della fattura. Si attiva quando lo stato dell'ordine è impostato come pagato, automaticamente oppure dal pannello di configurazione manualmente, se l'ordine non ha una fattura creata tramite Prestashop
    public function hookActionOrderStatusUpdate($order) {
        if(!$this->active || !$order['newOrderStatus']->paid || Configuration::get('PS_CREAZIONE_FATTURA') == 0 )
            return;
        $orderId = $order['id_order'];
        $check_order = new Order($orderId);
        if($check_order->hasInvoice())
            return;
        $query = 'SELECT docIdFattura24 FROM '._DB_PREFIX_.'orders WHERE id_order=\'' . $orderId . '\';';
        $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];
        if ($docIdFattura24 == NULL || curl_error()) // dovrebbe consentire di ricreare il documento in caso di errore cUrl
            $this->afterHook($check_order, true);
    }
    // Dato che voglio creare una fattura in Fattura24 solo se non è stata già creata in Prestashop, 
    // ho il problema che durante la creazione non conosco il numero della fattura per nominare il file nel modo corretto. 
    // Un modo di risolvere è usare questo hook per rinominare il file appena creato dall'hook hookActionOrderStatusUpdate con il nome corretto
    public function hookActionOrderStatusPostUpdate($order) {
        if(!$this->active || !$order['newOrderStatus']->paid || Configuration::get('PS_CREAZIONE_FATTURA') != 1 )
            return;
        $orderId = $order['id_order'];
        $check_order = new Order($orderId);
        $number = $check_order->invoice_number;
        if(empty($number))
            return;

        //[30.08.2018]
        // Gaetano
        //Qui costruisco il nome del file, contenente il progressivo fattura e l'anno che applicherò quando chiamo la funzione rename.
        $newFileName = $this->constructFileName($number,$check_order->date_add);
        
        //[30.08.2018]
        // Gaetano
        // Qui ricostruisco il nome del file pdf della fattura da rinominare
        // presente nella cartella di Download di Fattura24 contenuta nei file di Prestashop
        // con il progressivo fattura azzerato perchè quando viene creato, il plugin di Fattura24 non sa che numero assegnargli.
        $year = date("Y", strtotime($check_order->date_add));
        $oldFileName = 'FA000000-'. $year .'.pdf';
        
        //Qui viene fatta la rename del pdf
         $folder = _PS_DOWNLOAD_DIR_ . 'Fattura24/';
        rename($folder . $oldFileName, $folder . $newFileName);
    }

    // [30.08.2018]
    // Gaetano
    // Funzione generica per costruire o ricavare il nome della fattura pdf con il progressivo fattura e l'anno
    public function constructFileName($invoice_number, $date){
        $year = date("Y", strtotime($date));
        return 'FA' . sprintf('%06d', $invoice_number) . '-' . $year . '.pdf';
    }

    public function createCustomerXml($order_id){
        $this->trace("Create Customer Xml");
    	$domtree = new DOMDocument('1.0', 'UTF-8');
    	$domtree->preserveWhiteSpace = false;
    	$domtree->formatOutput = true;
    	$xmlRoot = $domtree->createElement("Fattura24");
    	$xmlRoot = $domtree->appendChild($xmlRoot);
    	$xmlCustomer = $domtree->createElement("Document");
    	$xmlCustomer = $xmlRoot->appendChild($xmlCustomer);
    	
    	$invoice_address = new Order($order_id);
        $invoice_address = $invoice_address->id_address_invoice;
        
    	$ga = new Address($invoice_address);
        $customer = new Customer($ga->id_customer);
        /**
         * Recupera i campi SDI e PEC per fatturazione elettronica
         * Davide Iandoli 08.03.2019
         */
        $sql = "select id_fattura24,fattura24_codice_destinatario,fattura24_pec from `"._DB_PREFIX_."fattura24` where id_customer = ".$ga->id_customer."";
        $result = Db::getInstance()->getRow($sql);
        $pec = $result['fattura24_pec'];
        $sdi_code = $result['fattura24_codice_destinatario'];
        
        /* controllo su codice destinatario */
        $isoCountry = Country::getIsoById($ga->id_country);

        if($isoCountry != 'IT'){
            $sdi_code = 'XXXXXXX';
        }
        elseif(empty($result['fattura24_codice_destinatario'])) {
            $sdi_code = '0000000';
        } else {
            $sdi_code = $result['fattura24_codice_destinatario'];
        }
        
        $customer_fePec = $domtree->createElement('FeCustomerPec');
        $customer_fePec->appendChild($domtree->createCDataSection($pec));
        $xmlCustomer->appendChild($customer_fePec);
             
        $customer_feCodiceDestinatario = $domtree->createElement('FeDestinationCode');
        $customer_feCodiceDestinatario->appendChild($domtree->createCDataSection($sdi_code));
        $xmlCustomer->appendChild($customer_feCodiceDestinatario);
           
    	if(!empty($ga->company) && !empty($ga->vat_number))
    		$customername = $ga->company;
        else
    		$customername = $ga->firstname . ' ' . $ga->lastname;
        $customer_name = $domtree->createElement('CustomerName');
        $customer_name->appendChild($domtree->createCDataSection($customername));
        $xmlCustomer->appendChild($customer_name);
        $customer_address = $domtree->createElement('CustomerAddress');
        $customer_address->appendChild($domtree->createCDataSection($ga->address1 . ($ga->address2 == null ? "" : " " . $ga->address2)));
        $xmlCustomer->appendChild($customer_address);
    	$xmlCustomer->appendChild($domtree->createElement('CustomerPostcode', $ga->postcode));
    	$xmlCustomer->appendChild($domtree->createElement('CustomerCity', $ga->city));
        
        if (!empty($ga->id_state)){
    		$state = new State($ga->id_state);
    		$xmlCustomer->appendChild($domtree->createElement('CustomerProvince', $state->iso_code));
    	}
    	$xmlCustomer->appendChild($domtree->createElement('CustomerCountry', Country::getIsoById($ga->id_country)));
        
        if(!empty($ga->dni)){   
            $customer_fiscalcode = $domtree->createElement('CustomerFiscalCode');
            $customer_fiscalcode->appendChild($domtree->createCDataSection(strtoupper($ga->dni)));
            $xmlCustomer->appendChild($customer_fiscalcode);
        }    
        
        if(!empty($ga->vat_number)){  
            $customer_vatcode = $domtree->CreateElement('CustomerVatCode');
            $customer_vatcode->appendChild($domtree->createCDataSection($ga->vat_number));
            $xmlCustomer->appendChild($customer_vatcode);
        }    

        if(!empty($ga->phone) || !empty($ga->phone_mobile)){
            $customer_cell_phone = $domtree->createElement('CustomerCellPhone');

            if(!empty($ga->phone))
                $customer_cell_phone->appendChild($domtree->createCDataSection($ga->phone));
            elseif(!empty($ga->phone_mobile))
                $customer_cell_phone->appendChild($domtree->createCDataSection($ga->phone_mobile));
            
            $xmlCustomer->appendChild($customer_cell_phone);
        } 
        
        $customer_email = $domtree->createElement('CustomerEmail');
        $customer_email->appendChild($domtree->createCDataSection($customer->email));
        $xmlCustomer->appendChild($customer_email);
    	return $domtree->saveXML();
    }

    public function createInvoiceXml($order_id, $isInvoice){
        $fixnum = function($n, $p){
            return number_format($n, $p, '.', ''); // modifica metodo arrotondamento senza separatore di decimali
        };
           
        $domtree = new DOMDocument('1.0', 'UTF-8');
        $domtree->preserveWhiteSpace = false;
        $domtree->formatOutput = true;
        $xmlRoot = $domtree->createElement("Fattura24");
        $xmlRoot = $domtree->appendChild($xmlRoot);
        $fattura_invoice = $domtree->createElement("Document");
        $fattura_invoice = $xmlRoot->appendChild($fattura_invoice);

        $order = new Order($order_id);
        $invoice_address = $order->id_address_invoice;
        $ga = new Address($invoice_address);
        $customer = new Customer($ga->id_customer);

        /*
        * Recupero campi SDI e PEC fatturazione elettronica
        */
        $sql = "select id_fattura24,fattura24_codice_destinatario,fattura24_pec from `"._DB_PREFIX_."fattura24` where id_customer = ".(int)$order->id_customer."";
        $result = Db::getInstance()->getRow($sql);
        $pec = $result['fattura24_pec'];
        $sdi_code = $result['fattura24_codice_destinatario'];
        
        /* controllo Codice Destinatario */
        $isoCountry = Country::getIsoById($ga->id_country);

        if($isoCountry != 'IT'){
            $sdi_code = 'XXXXXXX';
        }
        elseif(empty($result['fattura24_codice_destinatario'])) {
            $sdi_code = '0000000';
        } else {
            $sdi_code = $result['fattura24_codice_destinatario'];
        }
       
        $customer_fePec = $domtree->createElement('FeCustomerPec');
        $customer_fePec->appendChild($domtree->createCDataSection($pec));
        $fattura_invoice->appendChild($customer_fePec);
        $customer_feCodiceDestinatario = $domtree->createElement('FeDestinationCode');
        $customer_feCodiceDestinatario->appendChild($domtree->createCDataSection($sdi_code));
        $fattura_invoice->appendChild($customer_feCodiceDestinatario);


        if(!empty($ga->company) && !empty($ga->vat_number))
            $customername = $ga->company;
        else
            $customername = $ga->firstname . ' ' . $ga->lastname;  
        
        $customer_name = $domtree->createElement('CustomerName');
        $customer_name->appendChild($domtree->createCDataSection($customername));
        $fattura_invoice->appendChild($customer_name);
        $customer_address = $domtree->createElement('CustomerAddress');
        $customer_address->appendChild($domtree->createCDataSection($ga->address1 . ($ga->address2 == null ? "" : " " . $ga->address2)));
        $fattura_invoice->appendChild($customer_address);
        $fattura_invoice->appendChild($domtree->createElement('CustomerPostcode', $ga->postcode));
        $customer_name = $domtree->createElement('CustomerCity');
        $customer_name->appendChild($domtree->createCDataSection($ga->city));
        $fattura_invoice->appendChild($customer_name);
        
        if (!empty($ga->id_state)){
            $state = new State($ga->id_state);
            $fattura_invoice->appendChild($domtree->createElement('CustomerProvince', $state->iso_code));
        }
        else
            $fattura_invoice->appendChild($domtree->createElement('CustomerProvince', ''));
        
        $isoCountry = Country::getIsoById($ga->id_country);
        $fattura_invoice->appendChild($domtree->createElement('CustomerCountry', $isoCountry));

        if(!empty($ga->dni)){   
            $customer_fiscalcode = $domtree->createElement('CustomerFiscalCode');
            $customer_fiscalcode->appendChild($domtree->createCDataSection(strtoupper($ga->dni)));
            $fattura_invoice->appendChild($customer_fiscalcode);
        }  

        if(!empty($ga->vat_number)){
            $customer_vatcode = $domtree->createElement('CustomerVatCode');
            $customer_vatcode->appendChild($domtree->createCDataSection($ga->vat_number));
            $fattura_invoice->appendChild($customer_vatcode);
        }else if ($isoCountry !== 'IT' && empty($ga->vat_number)){
            $customer_vatcode = $domtree->createElement('CustomerVatCode');
            $customer_vatcode->appendChild($domtree->createCDataSection($customername));
            $fattura_invoice->appendChild($customer_vatcode);
        }

        if(!empty($ga->phone) || !empty($ga->phone_mobile)){
            $customer_cell_phone = $domtree->createElement('CustomerCellPhone');
            
            if(!empty($ga->phone))
                $customer_cell_phone->appendChild($domtree->createCDataSection($ga->phone));
            elseif(!empty($ga->phone_mobile))
                $customer_cell_phone->appendChild($domtree->createCDataSection($ga->phone_mobile));
            
            $fattura_invoice->appendChild($customer_cell_phone);   
        }   
        
        $customer_email = $domtree->createElement('CustomerEmail');
        $customer_email->appendChild($domtree->createCDataSection($customer->email));
        $fattura_invoice->appendChild($customer_email);
        
        if(!$order->isVirtual() && $order->id_address_delivery != null){
            $delivery_address = new Address($order->id_address_delivery);            
            
            if(!empty($delivery_address->company))
                $customername = $delivery_address->company;
            else
                $customername = $delivery_address->firstname . ' ' . $delivery_address->lastname;
            
            $delivery_name = $domtree->createElement('DeliveryName');
            $delivery_name->appendChild($domtree->createCDataSection($customername));
            $fattura_invoice->appendChild($delivery_name);
            $address = $domtree->createElement('DeliveryAddress');
            $address->appendChild($domtree->createCDataSection($delivery_address->address1 . ($delivery_address->address2 == null ? "" : " " . $delivery_address->address2)));
            $fattura_invoice->appendChild($address);
            $fattura_invoice->appendChild($domtree->createElement('DeliveryPostcode', $delivery_address->postcode));
            $fattura_invoice->appendChild($domtree->createElement('DeliveryCity', $delivery_address->city));
            
            if (!empty($delivery_address->id_state)){
                $state = new State($delivery_address->id_state); // deve cercare la provincia nel $delivery_address
                $fattura_invoice->appendChild($domtree->createElement('DeliveryProvince', $state->iso_code));
            }
            $fattura_invoice->appendChild($domtree->createElement('DeliveryCountry', Country::getIsoById($delivery_address->id_country)));
        }
        
        $fattura_invoice->appendChild($domtree->createElement('TotalWithoutTax', $order->total_paid_tax_excl));
        $payment_method_name = $domtree->createElement('PaymentMethodName');
        $PS_payment_method = Module::getInstanceByName($order->module); // mi prendo il nome del metodo di pagamento da PS
        $orderPayment = $PS_payment_method->displayName; // uso il nome visualizzato dall'utente
        
        if(!empty($orderPayment) && strpos(strtolower($orderPayment), 'paypal') !== false)
            $payment_method_needle = 'paypal';
        else if(strpos(strtolower($orderPayment), 'amzpayments') !== false)   
            $payment_method_needle = 'amazon payments';
        else if (strpos(strtolower($orderPayment), 'braintree') !== false)  
            $payment_method_needle = 'braintree'; 
        else if (strpos(strtolower($orderPayment), 'payplug') !== false) 
            $payment_method_needle = 'payplug'; 
        else if (strpos(strtolower($orderPayment), 'maofreepostepay') !== false)  
            $payment_method_needle = 'postepay'; 
        else if (strpos(strtolower($orderPayment), 'stripe') !== false) 
            $payment_method_needle = 'stripe';
        else if (strpos(strtolower($orderPayment), 'payment') !== false || strpos(strtolower($orderPayment), 'wallet') !== false)    
            $payment_method_needle = 'pagamento smart o con carta';    
        else
            $payment_method_needle = '';    

        $fattEl = Configuration::get('PS_CREAZIONE_FATTURA')==1; // solo fattura elettronica
        /* 
        * 20.11.2018
        * Gaetano
        * Qui riempio i campi dei pagamenti, se sto creando una fattura elettronica controllando i nomi dei moduli di pagamento di prestashop di default (DA MIGLIORARE E COMPATIBILE SOLO CON PRESTASHOP 1.7) 
        */
        $Total = number_format($order->total_paid, 2, '.', ''); // totali a due decimali Davide Iandoli 22.02.2019
        
        
        $modulePayment = $PS_payment_method->name;

        if(!empty($payment_method_needle)){ // controllo i pagamenti elettronici tramite $payment_method_needle
            $uc_modulePayment = ucfirst($modulePayment); // mi prendo il nome del modulo, convertendo l'iniziale in maiuscola
            $FePaymentCode = 'MP08';
            $payment_method_name->appendChild($domtree->createCDataSection($uc_modulePayment));
            $fattura_invoice->appendChild($payment_method_name);
            $payment_method_description = $domtree->createElement('PaymentMethodDescription'); 

        }elseif($modulePayment == "bankwire"){
            $FePaymentCode = 'MP05';
            $bank_name = $PS_payment_method->owner;
            $payment_method_name = $domtree->createElement('PaymentMethodName');
            $payment_method_name->appendChild($domtree->createCDataSection($bank_name));
            $fattura_invoice->appendChild($payment_method_name);
                
            $bank_detail = $PS_payment_method->details; // Utilizzo il campo 'Bank owner'(Intestatario del conto) per prendermi il nome dell'istituto
            $payment_method_description = $domtree->createElement('PaymentMethodDescription');
            $payment_method_description->appendChild($domtree->createCDataSection($bank_detail)); // creo una nuova sezione CData nell'albero xml
            $fattura_invoice->appendChild($payment_method_description); // aggiungo il tag xml alla fattura
              

        }elseif ($modulePayment == "cheque"){    // aggiungo pagamento con assegno
            $FePaymentCode = 'MP02';
            $payment_method_name->appendChild($domtree->createCDataSection($orderPayment));
            $fattura_invoice->appendChild($payment_method_name);
            $payment_method_description = $domtree->createElement('PaymentMethodDescription'); 
            
            
        }else{
            $FePaymentCode = 'MP01';
            $payment_method_name->appendChild($domtree->createCDataSection($orderPayment));
            $fattura_invoice->appendChild($payment_method_name);
            $payment_method_description = $domtree->createElement('PaymentMethodDescription'); 
           
        }
        $fattura_invoice->appendChild($payment_method_description); // tag defined always
        if($isInvoice && $fattEl) {
            $fattura_invoice->appendChild($domtree->createElement("FePaymentCode", $FePaymentCode)); // add tag only in invoice
        }        
     
        $fattura_invoice->appendChild($domtree->createElement('VatAmount', ($order->total_paid_tax_incl) - ($order->total_paid_tax_excl)));
        $fattura_invoice->appendChild($domtree->createElement('Total', $Total));
        
        if ($isInvoice){
            $payments = $fattura_invoice->appendChild($domtree->createElement('Payments'));
            $payment = $payments->appendChild($domtree->createElement('Payment'));
            $payment->appendChild($domtree->createElement('Date', $this->now('Y-m-d')));
            $Amount = number_format($order->total_paid, 2, '.', ''); //due decimali per amount
            $payment->appendChild($domtree->createElement('Amount', $Amount));
            if (Configuration::get('PS_STATO_PAGATO') == 1)
                $payment->appendChild($domtree->createElement('Paid', 'true'));
            else if (Configuration::get('PS_STATO_PAGATO') == 2) {
                $modulePayment = $PS_payment_method->name;
                $bool_invoice_paid_status = !empty($payment_method_needle) ? 'true' : 'false'; // see also lines 599 and following
                $payment->appendchild($domtree->createElement('Paid', $bool_invoice_paid_status));
            } else 
                $payment->appendChild($domtree->createElement('Paid', 'false'));
        }
        
        $products = $fattura_invoice->appendChild($domtree->createElement('Rows'));
        $pdcIsConfigured=false;
        $idPdc = Configuration::get('PS_F24_PDC');
        if(!empty($idPdc) && $idPdc != 'Nessun Pdc')
            $pdcIsConfigured=true;
    
        foreach ($order->getProducts() as $product_detail){

             /**
             * Con questo blocco di codice creo degli array che mi servono a gestire gli sconti
             * e a dividerne l'importo per le varie aliquote IVA
             * N.b.: Questi cicli vengono eseguiti solo se il totale sconto di un ordine è > 0
             * Davide Iandoli 23.10.2019
             */
            if($order->total_discounts_tax_excl > 0) { //inizio if
          
                foreach ($order->getCartRules() as $cart_rule){ // se esiste un nome coupon utilizzo quello nella descrizione
                    $discount_name = $cart_rule['name'];
                    if(empty($discount_name))
                        $discount_name = 'Sconto '; // altrimenti la forzo
                }
                             
                $vat_code = intval($product_detail['tax_rate']);
                $total_products = $order->total_products; // totale come somma dei prezzi prodotto
                $discount_neat = $order->total_discounts_tax_excl; // totale sconto netto
                //$this->trace('sconto netto :', $discount_neat);
                //$this->trace('totale prodotti :', $total_products);
                 
                $quantity_row = $product_detail['product_quantity'];
                $quantity_per_price = $product_detail['total_price_tax_excl'];
                //$quantity_per_price = $product_detail['unit_price_tax_excl'] * $product_detail['product_quantity']; // prezzo totale
                            
                $row_total_price = $quantity_per_price / $total_products; // percentuale del prezzo sul totale
                $item_row_total_percent = number_format($row_total_price, 2, '.', '');
                $discount_splitted = $discount_neat * $row_total_price;
                $row_discount_splitted = number_format($discount_splitted, 2, '.', '');
                
                /**
                * Creo un array associativo per memorizzare tutte
                * le informazioni che mi servono del prodotto
                */
                $row_product[] = array('total_price' => $quantity_per_price,
                                       'total_percent' => $item_row_total_percent,
                                       'vat_code' => $vat_code,
                                       'discount_percent' => $row_discount_splitted);

                 
                $rate_products_array = array(); // output array
                /**
                * Qui creo un 2° array associativo utilizzando $vat_code come chiave,
                * per sommare i totali dei prodotti con la stessa aliquota e i totali percentuali
                * (prezzo prodotti / totale e importo sconto / totale). Davide Iandoli 23.10.2019
                */
                foreach($row_product as $n => $value) {
                    $vat_value = $value['vat_code'];
                   
                    if(!array_key_exists($vat_value, $rate_products_array)) {
                        $rate_products_array[$vat_value]['total_price'] = $value['total_price'];
                        $rate_products_array[$vat_value]['total_percent'] = $value['total_percent'];
                        $rate_products_array[$vat_value]['discount_percent'] = $value['discount_percent'];
                   
                    } else {
                        $rate_products_array[$vat_value]['total_price'] += $value['total_price'] ;
                        $rate_products_array[$vat_value]['total_percent'] += $value['total_percent'] ;
                        $rate_products_array[$vat_value]['discount_percent'] += $value['discount_percent'];
                    }
                }                   
            } // fine if($order->total_discounts_tax_excl > 0)
            $this->trace('item details', $product_detail);
            
            $product = $products->appendChild($domtree->createElement('Row'));
            $code = $domtree->createElement('Code');
            $code->appendChild($domtree->createCDataSection($product_detail['product_reference'])); // correct product reference in fattura24 Davide Iandoli 31.01.2019
            $product->appendChild($code);
            $description = $domtree->createElement('Description');
            $description->appendChild($domtree->createCDataSection($product_detail['product_name']));
            $product->appendChild($description);
            $product->appendChild($domtree->createElement('Qty', $product_detail['product_quantity']));
            $product->appendChild($domtree->createElement('Price', $product_detail['unit_price_tax_excl']));
            $taxRate = $product_detail['tax_rate'];
            /*
            * 29.11.2018
            * Gaetano
            * Qui gestisco le aliquote che l'utente indica nella configurazione del modulo, per essere trattate come nature (da migliorare)
            */
            if(Configuration::get('PS_CREAZIONE_FATTURA') == 1){
                $idTaxRule = $product_detail['id_tax_rules_group'];
                if($idTaxRule == '0')
                    $product->appendChild($domtree->createElement('FeVatNature', 'N4') );
                elseif ($product_detail['tax_rate'] == '0.0') { // recupera natura IVA solo per prodotti ad aliquota zero
                    $naturaIvaStringConf = Configuration::get('PS_CONF_NATURA_IVA');
                    $naturaIvaArrayConf = explode(";", $naturaIvaStringConf); 
                    foreach($naturaIvaArrayConf as $oneNaturaConf){
                        if( (strpos($oneNaturaConf, $idTaxRule) !== false) && $taxRate == 0){
                            $oneNaturaConfArray = explode("-", $oneNaturaConf); 
                            $naturaToSend = $oneNaturaConfArray[1];
                            $product->appendChild($domtree->createElement('FeVatNature', $naturaToSend) );
                        }
                        else
                            continue;
                    }
                }
            }
            $product->appendChild($domtree->createElement('VatCode', $taxRate));
            $taxName = Product::getTaxesInformations($product_detail)['tax_name'];
            if(!empty($taxName))
                $product->appendChild($domtree->createElement('VatDescription', $taxName));
            if($pdcIsConfigured)
                $product->appendChild($domtree->createElement('IdPdc', $idPdc));
        }
    if(isset($rate_products_array)){
        foreach ($rate_products_array as $rate_key => $rate_values) {
                                                  
            $product = $products->appendChild($domtree->createElement('Row'));
            $code = $domtree->createElement('Code');
            $code->appendChild($domtree->createCDataSection($discount_name));
            $product->appendChild($code);
            $description = $domtree->createElement('Description');
            $description->appendChild($domtree->createCDataSection($discount_name . ' IVA '. $rate_key . '%'));
            $product->appendChild($description);
            $product->appendChild($domtree->createElement('Qty', 1));
            $product->appendChild($domtree->createElement('Price', - $rate_values['discount_percent']));
            $product->appendChild($domtree->createElement('VatCode', $rate_key));
            $product->appendChild($domtree->createElement('VatDescription', ' IVA '. $rate_key . '%')); // added Vat description
             /**
             * Per aliquote pari a 0% devo gestire la natura IVA, utilizzo la logica implementata per i prodotti
             * poiché le aliquote sono accorpate, su indicazione di Carlo invio solo una delle nature ignorando
             * le casistiche più complesse. Davide Iandoli 23.10.2019
             */
            if($rate_key == 0){
                $naturaIvaStringConf = Configuration::get('PS_CONF_NATURA_IVA');
                $naturaIvaArrayConf = explode(";", $naturaIvaStringConf); 
                foreach($naturaIvaArrayConf as $oneNaturaConf){
                    if(strpos($oneNaturaConf, $idTaxRule) !== false){
                        $oneNaturaConfArray = explode("-", $oneNaturaConf); 
                        $naturaToSend = $oneNaturaConfArray[1];
                        $product->appendChild($domtree->createElement('FeVatNature', $naturaToSend) );
                    }
                    else
                        continue;
                }
             } 
             if($pdcIsConfigured)
                $product->appendChild($domtree->createElement('IdPdc', $idPdc));
        }
    }        
    
    if ($order->total_shipping_tax_excl > 0){
        $shipping_company = Db::getInstance()->getRow("SELECT `name` FROM `" . _DB_PREFIX_ . "carrier` WHERE `id_carrier` = '" . pSQL($order->id_carrier) . "'");
        $product = $products->appendChild($domtree->createElement('Row'));
        $description = $domtree->createElement('Description');
        $description->appendChild($domtree->createCDataSection($this->l('Costi di spedizione:') . ' ' . $shipping_company['name']));
        $product->appendChild($description);
        $product->appendChild($domtree->createElement('Qty', '1'));
        $product->appendChild($domtree->createElement('Price', $order->total_shipping_tax_excl));
        $shippingRate = number_format($order->carrier_tax_rate, 0, '.', '');
        $product->appendChild($domtree->createElement('VatCode', $shippingRate));
        
             
        /**
        * Con questo blocco gestisco i casi in cui l'aliquota IVA
        * per la spedizione è 0%, inserendo la Natura
        */
        if ($shippingRate == 0){
            $naturaSpedizione = Configuration::get('PS_CONF_NATURA_SPEDIZIONE');
                   
            //$lista = $this->getListaNature_new(); // recupero la lista delle nature - nuovo metodo
            $lista = $this->getListaNature(); // recupero l'array delle nature
            $needle = substr($naturaSpedizione, 0, 2); // questa variabile mi serve a gestire le 'sottonature', se ci sono
            
            if ($naturaSpedizione == 'N0'){
                $naturaSpedizione = '';
                $VatDescription = 'IVA';
            } elseif (is_array($lista[$needle])) {
                $VatDescription = $lista[$needle][$naturaSpedizione]; // qui gestirò i casi in cui la Natura è N3.2 (esempio)
            } else {
                $VatDescription = $lista[$naturaSpedizione];
            }
        }
            
        $product->appendChild($domtree->createElement('VatDescription', $VatDescription)); // Descrizione in base all'opzione scelta
        if($shippingRate == 0)
            $product->appendChild($domtree->createElement('FeVatNature', $naturaSpedizione )); // Natura Iva in base all'opzione scelta
        if($pdcIsConfigured)
            $product->appendChild($domtree->createElement('IdPdc', $idPdc));
        }

        if ($isInvoice){
            $footnotes = $domtree->createElement('FootNotes');
            $footnotes->appendChild($domtree->CreateCDataSection($this->l('Rif. ordine: ') . $order->reference));
            $fattura_invoice->appendChild($footnotes);
            //$fattura_invoice->appendChild($domtree->createElement('FootNotes', 'Rif. ordine: ' . $order->reference));
            $f24_inv_object = Configuration::get('PS_INV_OBJECT'); // recupera la stringa dalle impostazioni
            $f24_search = array('(N)', '(R)'); // con questo array lascio decidere al cliente se vuole l'id dell'ordine, il rif. prestashop, oppure entrambi
            $f24_replace = array($order->id, $order->reference); // array di risultati con cui può essere sostituita la stringa
            $f24_send_object = str_replace($f24_search, $f24_replace, $f24_inv_object); 
            $f24_xml_object = $domtree->createElement('Object');
            $f24_xml_object->appendChild($domtree->createCDataSection($f24_send_object));
            $fattura_invoice->appendChild($f24_xml_object);
            //$fattura_invoice->appendChild($domtree->createElement('Object', $f24_send_object));
            
            /*
            * [20.11.2018]
            * Gaetano
            * Se attivo dal pannello di configurazione del modulo la disattivazione delle ricevute per i clienti non in posssesso di partita iva, creo lo stesso la fattura
            */
            if(empty($ga->vat_number)){
                if(Configuration::get('PS_CREAZIONE_FATTURA')==3)
                    $documentType = 'R';
                if(Configuration::get('PS_CREAZIONE_FATTURA')==2)
                    $documentType = Configuration::get('PS_DISABILITA_RICEVUTE') == 1 ? 'I-force' : 'R';
                if(Configuration::get('PS_CREAZIONE_FATTURA')==1)
                    $documentType = Configuration::get('PS_DISABILITA_RICEVUTE') == 1 ? 'FE' : 'R'; // traditional invoice if false 
            }
            else{
                if(Configuration::get('PS_CREAZIONE_FATTURA')==3)
                    $documentType = 'R';
                if(Configuration::get('PS_CREAZIONE_FATTURA')==2)
                    $documentType = "I";
                if(Configuration::get('PS_CREAZIONE_FATTURA')==1)
                    $documentType = "FE";
            }

            //se il cliente non ha una partita iva uso il sezionale ricevuta
            if(empty($ga->vat_number) || $documentType == 'R') // receipt
                $idNumerator = Configuration::get('PS_F24_SEZIONALE_RICEVUTA');
            else // uso il sezionale fattura
                $idNumerator = Configuration::get('PS_F24_SEZIONALE_FATTURA');
            if($idNumerator != 'Predefinito')
                $fattura_invoice->appendChild($domtree->createElement('IdNumerator', $idNumerator));

            $query = 'SELECT docIdOrderFattura24 FROM '._DB_PREFIX_.'orders WHERE id_order=\'' . $order_id . '\';';
            $docIdOrderFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdOrderFattura24'];
            if($docIdOrderFattura24 != null)
                $fattura_invoice->appendChild($domtree->createElement('F24OrderId', $docIdOrderFattura24));
            $sendEmail = Configuration::get('PS_EMAIL_FATTURA') == 1 ? 'true' : 'false';
            $updateStorage = Configuration::get('PS_F24_MAGAZZINO_FATTURA');
            if($order->isVirtual())
                $idTemplate = Configuration::get('PS_F24_MODELLO_FATTURA');
            else
                $idTemplate = Configuration::get('PS_F24_MODELLO_FATTURA_DEST');
        }
        else {
            $fattura_invoice->appendChild($domtree->createElement('Number', $order->id));
            $documentType = 'C';
            $sendEmail = Configuration::get('PS_MAIL_ORDINE') == 1 ? 'true' : 'false';
            $updateStorage = Configuration::get('PS_F24_MAGAZZINO_ORDINE');
            if($order->isVirtual())
                $idTemplate = Configuration::get('PS_F24_MODELLO_ORDINE');
            else
                $idTemplate = Configuration::get('PS_F24_MODELLO_ORDINE_DEST');
        }
        $fattura_invoice->appendChild($domtree->createElement('DocumentType', $documentType));

        /**
         * Con questo blocco riporto il bollo virtuale per le FE
         * e scrivo il tag solo se il valore è "V"
         * tre condizioni: opzione selezionata + tipo documento + totale > 77.47
         * Davide Iandoli 16.04.2020
         */
        $bollo_option = Configuration::get('PS_F24_BOLLO_VIRTUALE_FE');
        if($bollo_option == 1 && $documentType == 'FE' && $Total > 77.47) 
            $bollo_fe = "V";
        else
            $bollo_fe = "N";
                
        //$this->trace('bollo FE :', $bollo_fe);
        if ($bollo_fe == 'V')            
            $fattura_invoice->appendChild($domtree->createElement('FeVirtualStamp', $bollo_fe));
        // fine blocco bollo virtuale    
        $fattura_invoice->appendChild($domtree->createElement('SendEmail', $sendEmail));
        if(!empty($updateStorage))
            $fattura_invoice->appendChild($domtree->createElement('UpdateStorage', $updateStorage));
        if(!empty($idTemplate) && $idTemplate !== 'Predefinito')
            $fattura_invoice->appendChild($domtree->createElement('IdTemplate', $idTemplate));
        return $domtree->saveXML();
    }

    // download del documento
    public function downloadDocument($docIdFattura24, $orderId, $isInvoice){
        $fattura24_api_url = $this->baseUrl.'/api/v0.3/GetFile';
        $send_data = array();
        $send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $send_data['docId'] = $docIdFattura24;
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data));
        libxml_use_internal_errors(true);
        if (!simplexml_load_string(utf8_encode($dataReturned))){
            if($isInvoice){
                $order = new Order((int)$orderId);
                $fileName = $this->constructFileName($order->invoice_number,$order->date_add);
            }
            else
                $fileName = 'ORD'. sprintf('%06d', $orderId) . '.pdf';
            file_put_contents(_PS_DOWNLOAD_DIR_ . 'Fattura24/' . $fileName, $dataReturned);
        }
    }

    // salva contatto in rubrica F24
    public function saveCustomer($check_order){
    	$fattura24_api_url = $this->baseUrl.'/api/v0.3/SaveCustomer';
    	$send_data = array();
    	$send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
    	$send_data['xml'] = $this->createCustomerXml($check_order->id);
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data));
        
        $response = new DOMDocument();
        $response->preserveWhiteSpace = false;
        $response->formatOutput = true;
        $response->loadXML($dataReturned);
        $response_string = $response->saveXML();
        $this->trace($send_data['xml'], $response_string);
    }

    /* 
     * Funzione comune da eseguire dopo gli hook dichiarati sopra,
     * il parametro isInvoice serve per distinguere se deve essere creato un ordine o una fattura
     */
    public function afterHook($order, $isInvoice){
        $orderId = $order->id;
        $fattura24_api_url = $this->baseUrl.'/api/v0.3/SaveDocument';
        $send_data = array();
        $send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        if ($order->total_paid !=0) 
            $send_data['xml'] = $this->createInvoiceXml($orderId, $isInvoice); // non crea fattura con ordine a zero Davide Iandoli 31.01.2019
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data));
        $obj_dataReturned = simplexml_load_string(utf8_encode($dataReturned));
        $json_dataReturned = json_encode($obj_dataReturned);

        if(!$isInvoice){
            $column = 'apiResponseOrder';
        } else {
            $column = 'apiResponseInvoice';
        }

        if(is_object($obj_dataReturned)){
            $docIdFattura24 = $obj_dataReturned->docId;
            $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET '. $column .'=\'' . $json_dataReturned . '\' WHERE id_order=\'' . $orderId . '\';';
            Db::getInstance()->Execute($query);

        } else {
            $docIdFattura24 = explode('</docId>', explode('<docId>', $dataReturned)[1])[0];
            $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET '. $column .'=\'' . $json_dataReturned . '\' WHERE id_order=\'' . $orderId . '\';';
            Db::getInstance()->Execute($query);
        }

        if(!empty($docIdFattura24)){
            if (!$isInvoice){ 
                $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET docIdOrderFattura24=\'' . $docIdFattura24 . '\' WHERE id_order=\'' . $orderId . '\';';
                Db::getInstance()->Execute($query);
                $this->downloadDocument($docIdFattura24, $orderId, false);
            }
            else {
                $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET docIdFattura24=\'' . $docIdFattura24 . '\' WHERE id_order=\'' . $orderId . '\';';
                Db::getInstance()->Execute($query);
                $this->downloadDocument($docIdFattura24, $orderId, true); 
            }
        }

        $response= new DOMDocument();
        $response->preserveWhiteSpace = false;
        $response->formatOutput = true;
        $response->loadXML($dataReturned);
        $response_string = $response->saveXML();
        $this->trace($send_data['xml'], $response_string);
    }

    public function uninstall(){
        if (!parent::uninstall())
            return false;
        return true;
    }
 
    /**
     * Hook aggiuntivo per la gestione dei campi PEC e SDI lato admin
     * Davide Iandoli 08.05.2019
     */
    public function hookDisplayAdminCustomers($params)
    {
        if (substr(_PS_VERSION_,0,3) >= 1.7) {
            $idc = $params['id_customer'];
        } else {
            $idc = pSQL(tools::getValue('id_customer'));
        }
        
        $sdi_code = Tools::getValue('fattura24_codice_destinatario');
        if (!$sdi_code or strlen($sdi_code) <= 1)
        {
            $sdi_code = '0000000';    
        }
        if (Tools::getValue('id_fattura24')>0)
        {
        $sql = "update `"._DB_PREFIX_."fattura24` set fattura24_codice_destinatario = '".pSQL($sdi_code)."',fattura24_pec='".pSQL(Tools::getValue('fattura24_pec'))."'
        where id_fattura24 = ".(int)Tools::getValue('id_fattura24')." "; 
                   Db::getInstance()->Execute($sql);
        } else {
        $sql = "INSERT INTO `"._DB_PREFIX_."fattura24`(id_customer,fattura24_codice_destinatario,fattura24_pec) 
            	   VALUES (".$idc.",'".pSQL($sdi_code)."','".pSQL(Tools::getValue('fattura24_pec'))."')";
                   Db::getInstance()->Execute($sql);
        }
        
        $sql = "select id_fattura24,fattura24_codice_destinatario,fattura24_pec from `"._DB_PREFIX_."fattura24` where id_customer = ".Tools::getValue('id_customer')."";
        $result = Db::getInstance()->getRow($sql);

        if (substr(_PS_VERSION_,0,5)>=1.7) {
            $linkController = Context::getContext()->link->getAdminLink('AdminCustomers', true, [], [
                'id_customer' => $idc,
                'viewcustomer' => 1
            ]);
        } else {
		  $linkController = $this->context->link->getAdminLink("AdminCustomers", true) . "&id_customer=".$idc."&viewcustomer";
        }

        $this->context->smarty->assign('linkController', $linkController);
        $this->context->smarty->assign('id_fattura24', $result['id_fattura24']);
        $this->context->smarty->assign('fattura24_codice_destinatario', $result['fattura24_codice_destinatario']);
        $this->context->smarty->assign('fattura24_pec', $result['fattura24_pec']);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/admin_customer.tpl');
        return $output;
    }
    
    /**
    * Hook per la gestione del contenuto della Tab
    * Utilizzo $fattura24_order_docId e $fattura24_invoice_docId
    * per visualizzare selettivamente i tasti in base alla presenza del docId nel db
    */
    public function hookDisplayAdminOrder($params)
    {
        $orderId = Tools::getValue('id_order'); // mi prendo l'id dell'ordine per le query sui docId
        $order = new Order($orderId);
        $order_status = $order->getCurrentOrderState();
        if(is_object($order_status)){
            if($order_status->name[1] == 'Annullato')
                $order_status_name = '';
            else
                $order_status_name = $order_status->name[1];    
        }else{
            $order_status_name = $order_status;
        }
        $query = 'SELECT docIdFattura24, docIdOrderFattura24, apiResponseOrder, apiResponseInvoice FROM '._DB_PREFIX_.'orders WHERE id_order=\'' . $orderId . '\';';
        $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24'];   
        $docIdOrderFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdOrderFattura24'];
        $apiResponseOrder = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['apiResponseOrder'];
        $apiResponseInvoice = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['apiResponseInvoice'];    
        $obj_apiResponseOrder = json_decode($apiResponseOrder);
        $obj_apiResponseInvoice = json_decode($apiResponseInvoice);
        $orderResponseDescription = is_object($obj_apiResponseOrder)? $obj_apiResponseOrder->description : $obj_apiResponseOrder['description'];
        $invoiceResponseDescription = is_object($obj_apiResponseInvoice)? $obj_apiResponseInvoice->description : $obj_apiResponseInvoice['description'];
        $config_CreaOrdine =  Configuration::get('PS_CREAZIONE_ORDINE');
        $config_CreaFattura =  Configuration::get('PS_CREAZIONE_FATTURA');

        // uso order_documents per incrementare il contatore
        $order_documents = 0;
        if(!empty($docIdFattura24 || strpos($invoiceResponseDescription, 'already exists') !== false))
           $order_documents++;
        if(!empty($docIdOrderFattura24 || strpos($orderResponseDescription, 'already exists') !== false))
           $order_documents++;   
                
        if(!$config_CreaOrdine){
            $message_order = 'creazione documento disabilitata';
        } else if(empty($order_status_name)){
            $message_order = 'ordine annullato in Prestashop';
        } else {
            if(!empty($docIdOrderFattura24 || strpos($orderResponseDescription, 'already exists') !== false))
                $message_order = 'documento salvato in Fattura24';
            else
                $message_order = 'documento NON salvato in Fattura24';
        }
    
        if(!$config_CreaFattura){
            $message_invoice = 'creazione documento disabilitata';
        } else if (empty($order_status_name)) {
            $message_invoice = 'ordine annullato in Prestashop';
        } else {
            if(!empty($docIdFattura24 || strpos($invoiceResponseDescription, 'already exists') !== false))
                $message_invoice = isset($obj_apiResponseInvoice->docNumber)? 'documento n. '. $obj_apiResponseInvoice->docNumber . ' salvato in Fattura24' :'documento salvato in Fattura24';
            else
                $message_invoice = 'documento NON salvato in Fattura24';
        }

         
        $this->context->smarty->assign('id_order', $orderId);
        $this->context->smarty->assign('order_status_name', $order_status_name); //se l'ordine è annullato nascondo i pulsanti
        $this->context->smarty->assign('config_CreaOrdine', $config_CreaOrdine);
        $this->context->smarty->assign('config_CreaFattura', $config_CreaFattura);
        $this->context->smarty->assign('order_documents', $order_documents); // contatore documenti salvati
        $this->context->smarty->assign('fattura24_order_docId', $docIdOrderFattura24); //docId per tasto ordine
        $this->context->smarty->assign('fattura24_order', $message_order);
        $this->context->smarty->assign('fattura24_invoice_docId', $docIdFattura24); //docId per tasto fattura
        $this->context->smarty->assign('fattura24_invoice', $message_invoice);
        
        return $this->display(__FILE__, 'views/templates/hook/admin_order.tpl');
    }

    // con questo metodo consento di ricreare l'ordine associando l'azione al pulsante nella tab Fattura24
    /*function smartyOrder($params){ // commentato per sviluppo pulsanti
        
        $config_CreaOrdine =  Configuration::get('PS_CREAZIONE_ORDINE');
        $orderId = Tools::getValue('id_order'); // mi prendo l'id dell'ordine per le query sui docId
        $check_order = new Order($orderId);

        $query = 'SELECT docIdFattura24, docIdOrderFattura24, apiResponseOrder, apiResponseInvoice FROM '._DB_PREFIX_.'orders WHERE id_order=\'' . $orderId . '\';';
        $docIdOrderFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdOrderFattura24'];  
        $apiResponseOrder = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['apiResponseOrder'];  
        $apiResponseInvoice = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['apiResponseInvoice'];  
        $obj_apiResponseOrder = json_decode($apiResponseOrder);
        $obj_apiResponseInvoice = json_decode($apiResponseInvoice);

        $this->afterHook($check_order, false); // creo l'ordine usando il metodo afterHook
        
        if(!empty($docIdOrderFattura24)){
            $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET docIdOrderFattura24=\'' . $docIdOrderFattura24 . '\' WHERE id_order=\'' . $orderId . '\';';
            Db::getInstance()->Execute($query);
            $this->downloadDocument($docIdOrderFattura24, $orderId, true); 
        } else if (!empty($obj_apiResponseOrder->description)) { // con questa condizione popolo il docId anche nel caso in cui il documento è già esistente in F24
            $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET docIdOrderFattura24=\'' . $obj_apiResponseOrder->description . '\' WHERE id_order=\'' . $orderId . '\';';
            Db::getInstance()->Execute($query); 
        }
    }

    // con questo metodo consento di ricreare la fattura associando l'azione al pulsante nella tab Fattura24
    function smartyInvoice($params){
        
        $config_CreaFattura =  Configuration::get('PS_CREAZIONE_FATTURA');
        $orderId = Tools::getValue('id_order'); // mi prendo l'id dell'ordine per le query sui docId
        $check_order = new Order($orderId);
        $query = 'SELECT docIdFattura24, docIdOrderFattura24, apiResponseOrder, apiResponseInvoice FROM '._DB_PREFIX_.'orders WHERE id_order=\'' . $orderId . '\';';
        $docIdFattura24 = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['docIdFattura24']; 
        $apiResponseOrder = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['apiResponseOrder'];  
        $apiResponseInvoice = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query)['apiResponseInvoice'];  
        $obj_apiResponseOrder = json_decode($apiResponseOrder);
        $obj_apiResponseInvoice = json_decode($apiResponseInvoice);

        $this->afterHook($check_order, true); // creo la fattura usando il metodo afterHook
                
        if(!empty($docIdFattura24)){
            $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET docIdFattura24=\'' . $docIdFattura24 . '\' WHERE id_order=\'' . $orderId . '\';';
            Db::getInstance()->Execute($query);
            $this->downloadDocument($docIdFattura24, $orderId, false);
        }else if(!empty($obj_apiResponseInvoice->description)){ // con questa condizione popolo il docId anche nel caso in cui il documento è già esistente in F24
            $query = 'UPDATE ' . _DB_PREFIX_ . 'orders SET docIdFattura24=\'' . $obj_apiResponseInvoice->description . '\' WHERE id_order=\'' . $orderId . '\';';
            Db::getInstance()->Execute($query);
        }
    }*/
    
    /**
     * 26.11.2018
     * Gaetano
     * Con questo hook assegno i nuovi campi e aggiungo al form di registrazione del Cliente i nuovi campi.
     * Gestione modificata da Davide Iandoli 11.03.2019
     */
    public function hookDisplayCustomerAccountForm($params){
        
        if ($params['cookie']->id_customer)
        {
        $sql = "select id_fattura24,fattura24_codice_destinatario,fattura24_pec from `"._DB_PREFIX_."fattura24` where id_customer = ".$params['cookie']->id_customer."";
        $result = Db::getInstance()->getRow($sql);
        }

        $this->context->smarty->assign('id_fattura24', $result['id_fattura24']);
        $this->context->smarty->assign('fattura24_id_customer', $params['cookie']->id_customer);
        $this->context->smarty->assign('fattura24_codice_destinatario', $result['fattura24_codice_destinatario']);
        $this->context->smarty->assign('fattura24_pec', $result['fattura24_pec']);
        return $this->display(__FILE__, 'views/templates/hook/customer_reg_form.tpl');
        
    } 

    public function hookDisplayCustomerIdentityForm($params){
        
        $sdi_code = Tools::getValue('fattura24_codice_destinatario');
        if (!$sdi_code or strlen($sdi_code) <= 1)
        {
            $sdi_code = '0000000';    
        }

        if (Tools::getValue('id_fattura24')>0)
        {
            $sql = "update `"._DB_PREFIX_."fattura24` set fattura24_codice_destinatario = '".pSQL($sdi_code)."',fattura24_pec='".pSQL(Tools::getValue('fattura24_pec'))."' where id_fattura24 = ".(int)Tools::getValue('id_fattura24')." "; 
            Db::getInstance()->Execute($sql);
        
        } elseif (Tools::getValue('fattura24_id_customer')) {
            $sql = "INSERT INTO `"._DB_PREFIX_."fattura24`(id_customer,fattura24_codice_destinatario,fattura24_pec) 
            	   VALUES (".pSQL(Tools::getValue('fattura24_id_customer')).",'".pSQL($sdi_code)."','".pSQL(Tools::getValue('fattura24_pec'))."')";
                   Db::getInstance()->Execute($sql);
        }
        if ($params['cookie']->id_customer)
        {
            $sql = "select id_fattura24,fattura24_codice_destinatario,fattura24_pec from `"._DB_PREFIX_."fattura24` where id_customer = ".$params['cookie']->id_customer."";
            $result = Db::getInstance()->getRow($sql);
        }
        $this->context->smarty->assign('id_fattura24', $result['id_fattura24']);
        $this->context->smarty->assign('fattura24_id_customer', $params['cookie']->id_customer);
        $this->context->smarty->assign('fattura24_codice_destinatario', $result['fattura24_codice_destinatario']);
        $this->context->smarty->assign('fattura24_pec', $result['fattura24_pec']);
        return $this->display(__FILE__, 'views/templates/hook/customer_reg_form.tpl');
    }    
           
    /**
    * Gaetano
    * 26.11.2018
    * Con questo hook recupero i campi pec e destinatario dal form di registrazione
    * Gestione modificata da Davide Iandoli 11.03.2019
    */
    public function hookActionCustomerAccountAdd($params){
        
        $newCustomer = $params['newCustomer'];
        $sql = "INSERT INTO `"._DB_PREFIX_."fattura24`(id_customer,fattura24_codice_destinatario,fattura24_pec) 
           	   VALUES (".$newCustomer->id.",'".pSQL(Tools::getValue('fattura24_codice_destinatario'))."','".pSQL(Tools::getValue('fattura24_pec'))."')";
        Db::getInstance()->Execute($sql);

        if (0) {
            $handle = fopen(dirname(__FILE__).'/notify.html', 'a');
            fwrite($handle, "*********new*********<br>\n");
            fwrite($handle, 'start: '." \n<br>\n [".date('Y-m-d h-m-s')."]<br><br>\n");
            
            fwrite($handle, "pec:".Tools::getValue('fattura24_pec'));
            fwrite($handle, "codice_destinatario:".Tools::getValue('fattura24_codice_destinatario'));
            fwrite($handle, "customer:".$newCustomer->id);
            fwrite($handle, "sql:".$sql);
            
            fwrite($handle, 'end: '." \n<br>\n [".date('Y-m-d h-m-s')."]<br><br>\n");
            fclose($handle);
        }
    }

    public function hookActionCustomerAccountUpdate($params)
    {
        $sdi_code = Tools::getValue('fattura24_codice_destinatario');
        if (!$sdi_code or strlen($sdi_code) <= 1)
        {
           $sdi_code = '0000000';    
        }
      
        if (Tools::getValue('id_fattura24')>0)
        {
            $sql = "update `"._DB_PREFIX_."fattura24` set fattura24_codice_destinatario = '".pSQL($sdi_code)."',fattura24_pec='".pSQL(Tools::getValue('fattura24_pec'))."' where id_fattura24 = ".(int)Tools::getValue('id_fattura24')." "; 
            Db::getInstance()->Execute($sql);
        } elseif (Tools::getValue('fattura24_id_customer')) {
            $sql = "INSERT INTO `"._DB_PREFIX_."fattura24`(id_customer,fattura24_codice_destinatario,fattura24_pec) 
            	   VALUES (".pSQL(Tools::getValue('fattura24_id_customer')).",'".pSQL($sdi_code)."','".pSQL(Tools::getValue('fattura24_pec'))."')";
                   Db::getInstance()->Execute($sql);
        }

    }

    function checkNewVersion($urlCheckVersion){
        try{
            $fileCheckVersion = fopen($urlCheckVersion, "r");
            if($fileCheckVersion){
                $content = stream_get_contents($fileCheckVersion);
                fclose($fileCheckVersion);
                return $content;
            }
        }
        catch(Exception $e){
            // non succede nulla
        }
        return false;
    }

    function getVersionCheckMessage(){
        $versionCheckMessage = '';
        $urlCheckVersion = "https://www.fattura24.com/prestashop/latest_version_1.6.txt";
        $checkNewVersion = $this->checkNewVersion($urlCheckVersion);
        
        if($checkNewVersion){
            $latest_version_date = substr($checkNewVersion,0,16);
            if($latest_version_date !== $this->moduleDate){
                $urlDownload = substr($checkNewVersion,17);
                $versionCheckMessage = $this->l('È stata rilasciata una nuova versione del modulo!');
                return $this->displayWarning($versionCheckMessage);
            }
        }
        return '';
    }

    // lista modelli
    public function getTemplate($templates, $isOrder){
        $listaNomi = array();
        $listaNomi['Predefinito'] = 'Predefinito';
        $xml = simplexml_load_string(utf8_encode($templates));
        if (is_object($xml)){
            $listaModelli = $isOrder ? $xml->modelloOrdine : $xml->modelloFattura;
            foreach($listaModelli as $modello)
                $listaNomi[intval($modello->id)] = strval($modello->descrizione);
        }
        else
            $this->trace('error list templates', $templates);
        return $listaNomi;
    }
    
    // lista pdc
    public function getPdc($f24_pdc){
        $listaNomi = array();
        $listaNomi['Nessun Pdc'] = 'Nessun Pdc';
        $xml = simplexml_load_string(utf8_encode($f24_pdc));
        if (is_object($xml)){
            foreach($xml->pdc as $pdc)
                if(intval($pdc->ultimoLivello) == 1)
                    $listaNomi[intval($pdc->id)] = str_replace('^', '.', strval($pdc->codice)) . ' - ' . strval($pdc->descrizione);
        }
        else
            $this->trace('error list pdc', $f24_pdc);
        return $listaNomi;
    }


    // lista sezionali
    function GetNumerator($f24_sezionali, $idTipoDocumento){
        $listaNomi = array();
        $listaNomi['Predefinito'] = 'Predefinito';
        $xml = simplexml_load_string(utf8_encode($f24_sezionali));
        if (is_object($xml)){
            foreach($xml->sezionale as $sezionale)
                foreach($sezionale->doc as $doc)
                    if(intval($doc->id) == $idTipoDocumento && intval($doc->stato) == 1)
                        $listaNomi[intval($sezionale->id)] = strval($sezionale->anteprima);
        }
        else
            $this->trace('error list sezionale', $f24_sezionali);
        return $listaNomi;
    }

     /**
     * Con questo metodo gestisco la lista delle nature di esenzione
     * per l'aliquota di spedizione, sia nelle impostazioni del plugin
     * sia nell'ordine / fattura
     * Davide Iandoli 18.03.2020
     */
    function getListaNature() {

        $tipiAliquoteIva = ['N0' => 'Nessuna',
                            'N1' => 'escluse ex art. 15',
                            'N2' => 'non soggette',
                            'N3' => 'non imponibili',
                            'N4' => 'esenti',
                            'N5' => 'regime del margine / IVA non esposta in fattura',
                            'N6' => 'inversione contabile',
                            'N7' => 'IVA assolta in altro stato UE'];
        
        return $tipiAliquoteIva;                            
    }
    
    /**
    * Nuova lista nature in conformità con nuove specifiche AdE
    */
    /*function getListaNature_new() {
        
        $tipiAliquoteIva = [
        
        'N0' => 'Nessuna',
        'N1' => ['N1' => 'escluse ex art. 15'],
        'N2' => ['N2' => 'non soggette',
                 'N2.1' => 'non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72',
                 'N2.2' => 'altri casi'],
        'N3' => ['N3' => 'non imponibili',
                 'N3.1' => 'esportazioni',
                 'N3.2' => 'cessioni intracomunitarie',
                 'N3.3' => 'cessioni verso San Marino',
                 'N3.4' => 'operazioni assimilate alle cessioni all\'esportazione',
                 'N3.5' => 'a seguito di dichiarazioni d\'intento',
                 'N3.6' => 'altre operazioni che non concorrono alla formazione del plafond'],
        'N4' => ['N4' => 'esenti'],
        'N5' => ['N5' => 'regime del margine / IVA non esposta in fattura'],
        'N6' => ['N6' => 'inversione contabile',
                 'N6.1' => 'cessione di rottami e altri materiali di recupero',
                 'N6.2' => 'cessione di oro e argento puro',
                 'N6.3' => 'subappalto nel settore edile',
                 'N6.4' => 'cessione di fabbricati',
                 'N6.5' => 'cessione di telefoni cellulari',
                 'N6.6' => 'cessione di prodotti elettronici',
                 'N6.7' => 'prestazioni comparto edile e settori connessi',
                 'N6.8' => 'operazioni settore energetico',
                 'N6.9' => 'altri casi'],
        'N7' => ['IVA assolta in altro stato UE']
        ];
        
        return $tipiAliquoteIva;         
    }*/
    
    // ottiene il timestamp con il fuso orario di Roma
    function now($fmt = 'Y-m-d H:i:s', $tz = 'Europe/Rome'){
        $timestamp = time();
        $dt = new \DateTime("now", new \DateTimeZone($tz));
        $dt->setTimestamp($timestamp);
        return $dt->format($fmt);
    }
    
    // usa la funziona cUrl per le chiamate API
    public function curlDownload($url, $data_string){
        if(!function_exists('curl_init')){
            $this->trace('curl is not installed');
            die('Sorry, cURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $config = array();
        $config['useragent'] = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';
        curl_setopt($ch, CURLOPT_USERAGENT, $config['useragent']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        
        if(!($output = curl_exec($ch)))
            $this->trace('curl error', curl_getinfo($ch), curl_error($ch), curl_errno($ch));
        curl_close($ch);
        return $output;
    }

    /**
     *  Con questo metodo mi prendo le aliquote di spedizione
     *  in base alla lingua utilizzata nello shop; 
     *  mi serve per gestire il messaggio di errore per il campo
     *  Natura spedizione. 
     *  Davide Iandoli 12.06.2020
     */
    function f24Carriers()
    {
        $context = Context::getContext();
        $id_lang = $context->language->id;
        $f24_carriers = Carrier::getCarriers($id_lang);
        $carrier_tax_rate = array();

        foreach ($f24_carriers as $key => $val) {
            $id_carrier = $val['id_carrier'];
            if ((int)Tax::getCarrierTaxRate($id_carrier) == 0) {
                $carrier_tax_rate[$id_carrier] = Tax::getCarrierTaxRate($id_carrier);
            }    
        }
        return $carrier_tax_rate;
    }

    function getZeroRates()
    {
        $tax_rates = Tax::getTaxes();
        $zero_rate = array();
        
        foreach ($tax_rates as $k => $v) {
            if ((int)$v['rate'] == 0) {
                $id_tax =  $v['id_tax'];
                $zero_rate[$id_tax] = $v['rate'];
            }
        }
        return $zero_rate;
    }

    /**
     * Con queto metodo gestisco i messaggi di errore
     * per la Fe e la mancata configurazione della natura IVA
     * Davide Iandoli 12.06.2020
     */
    function getNaturaError() {
        $zeroRates = $this->getZeroRates(); // aliquote prodotti
        $zeroShipping = $this->f24Carriers(); // aliquote spedizione
        $fattEl = Configuration::get('PS_CREAZIONE_FATTURA') == 1; // solo per fatture elettroniche
        $confNatura = empty(Configuration::get('PS_CONF_NATURA_IVA'));
        $confSped = Configuration::get('PS_CONF_NATURA_SPEDIZIONE') == 'N0' && !empty($zeroShipping);
        $errMsg = '';
        
        // messaggio differenziato a seconda dei casi
        if (!empty($zeroRates) && $fattEl) {
            if ($confNatura) {
                if ($confSped) {
                    $errMsg = $this->l('Devi impostare una Natura per entrambi i campi');
                } else {
                    $errMsg = $this->l('Devi impostare una Natura per ogni aliquota 0%');
                }    
            } else if ($confSped) {
                $errMsg = $this->l('Devi impostare una Natura spedizione per ogni aliquota 0%'); 
            } 
        }
        
        if (!empty($errMsg)) {
            return $this->displayError($errMsg); // uso i metodi di prestashop
        } else {
            return '';
        }    
    }
    
    // test chiave API
    public function testApiKey(){
        $fattura24_api_url = $this->baseUrl.'/api/v0.3/TestKey';
        $send_data = array();
        $send_data['apiKey'] = Configuration::get('PS_FATTURA24_API');
        $dataReturned = $this->curlDownload($fattura24_api_url, http_build_query($send_data));
        $xml = simplexml_load_string(str_replace('&egrave;', 'è', $dataReturned));
        $subscriptionTypeIsValid = true;
        $subscriptionDaysToExpiration = 365;
        if(is_object($xml)){
            $returnCode = intval($xml->returnCode);
            $description = strval($xml->description);
            if($returnCode == 1){
                $subscriptionType = intval($xml->subscription->type);
                if($subscriptionType != 5 && $subscriptionType != 6)
                    $subscriptionTypeIsValid = false;
                $subscriptionExpire = strval($xml->subscription->expire);
                $date1 = $this->now();
                $date2 = str_replace('/', '-', $subscriptionExpire);
                $diff = abs(strtotime($date1) - strtotime($date2));
                $subscriptionDaysToExpiration = ceil($diff / 86400);                
            }
        }
        else{
            $returnCode = '?';
            $description = 'Errore generico, per favore contatta il nostro servizio tecnico a info@fattura24.com';
        }
        return array(
            'returnCode' => $returnCode,
            'description' => $description,
            'subscriptionTypeIsValid' => $subscriptionTypeIsValid,
            'subscriptionDaysToExpiration' =>  $subscriptionDaysToExpiration
        );
    }
    
    // informazioni sull'ambiente (versione PHP, versione PS, etc. )
    function getInfo(){
        return 'Versione del modulo di Fattura24: ' . $this->version . '\nVersione di Prestashop: ' 
            . _PS_VERSION_ . '\nVersione di PHP: ' . PHP_VERSION . '\nUrl negozio: ' . _PS_BASE_URL_;
    }
    
    // scrive il tracciato del plugin
    function trace(){
        $fp = fopen(_PS_MODULE_DIR_. 'fattura24/log/trace.log' , "a+");
        fprintf($fp, "%s: %s\n\n", $this->now(), var_export(func_get_args(), true));
        fclose($fp);
    }

    // [30.08.2018]
    // Gaetano
    // Funzione di utilità per stampare variabili sul file di log del modulo, in assenza di un sistema di debug disponibile.
    function traceDebug($string){
        $fp = fopen(_PS_MODULE_DIR_. 'fattura24/log/traceDebug.log' , "a+");
        fprintf($fp, $string."\n");
        fclose($fp);
    }
}