# Changelog
## 1.7.11
###### _Lug 6, 2020_
- Aggiunti dati delle modalità di pagamento anche nell'ordine

## 1.7.10
###### _Giu 16, 2020_
- Aggiunto messaggio di errore in caso di FE e mancata configurazione della Natura per aliquote 0%

## 1.7.9
###### _Mag 18, 2020_
- Miglioramento prestazioni del plugin
- Aggiunta opzione per la creazione di ricevute
- Sostituito il link di accesso a Fattura24 con v3
## 1.7.8
###### _Apr 22, 2020_
- Aggiunta gestione del bollo virtuale per le FE
- Aggiunti metodi di pagamento elettronico
## 1.7.7
###### _Mar 23, 2020_
- Aggiunto il riferimento al numero della fattura (se esistente) nella colonna 'Stato fattura'
## 1.7.6
###### _Mar 12, 2020_
- Modificata la visualizzazione dello stato degli ordini/fatture in F24
- conversione del dato immesso nel campo c.f. in maiuscolo
- Rinomina del file trace.log al momento del download
## 1.7.3
###### _Feb 24, 2020_
- Corretto bug che non consentiva la modifica dei campi PEC e Codice Destinatario lato admin
## 1.7.2
###### _Feb 19, 2020_
- Revisione generale del codice
- Aggiunte sezioni CData ai tag xml Object e FootNotes
## 1.7.1
###### _Feb 05, 2020_
- Correzione bug: aggiunta descrizione aliquota IVA per costi di spedizione e coupon
- rimosso tag duplicato PaymentMethodName
## 1.7.0
###### _Gen 30, 2020_
- Migliorata la gestione dei metodi di pagamento: il tag FePaymentCode viene passato solo per le fatture
- Corretto bug relativo al sezionale delle ricevute
- Correzione bug minori
## 1.6.9
###### _Gen 20, 2020_
- modificata la gestione dei pagamenti elettronici
## 1.6.8
###### _Gen 15, 2020_
- Corretto bug nel checkout dei pagamenti elettronici per fatture tradizionali
## 1.6.7
###### _Dic 20, 2019_
- Cambiata la casella 'Stato Pagato' in un menu a tendina
## 1.6.6
###### _Dic 11, 2019_
- Creazione della fattura in stato 'Pagato' con metodi di pagamento come Paypal
## 1.6.53c
###### _Nov 22, 2019_
- Aggiunta la gestione del piano dei conti anche nell'ordine
## 1.6.52
###### _Nov 07, 2019_
- Aggiunta la possibilità di indicare il riferimento Prestashop nella causale del documento
## 1.6.51
###### _Ott 28, 2019_
- Migliorato l'aspetto e il comportamento della schermata impostazioni
- Cambiato il comportamento del campo partita iva per paesi esteri
- Migliorato il comportamento della Natura Iva per la spedizione
## 1.6.50
###### _Ott 23, 2019_
- Cambiata la gestione degli sconti: ora l'importo totale viene diviso per aliquota IVA
## 1.6.49
###### _Ott 15, 2019_
- Aggiunta colonna di visualizzazione dello stato dei documenti nel gestionale F24
- Aggiunta gestione della Natura IVA per aliquote di spedizione pari a 0%
- Modificata visualizzazione delle opzioni Natura Iva: ora sono visibili solo se è selezionata l'opzione Fattura Elettronica 
## 1.6.48
###### _Set 24, 2019_
- Migliorata gestione dati pagamento e Iban
- Cambiata la visualizzazione dell'elenco sezionali: ora viene visualizzata l'anteprima
## 1.6.47
###### _Lug 30, 2019_
- Aggiunto pulsante oggetto predefinito
## 1.6.46
###### _Lug 25, 2019_
- Aggiunta gestione modalità di pagamento stripe
## 1.6.45
###### _Lug 01, 2019_
- Aggiunto link novità AdE in fondo alla documentazione
## 1.6.44
###### _Mag 30, 2019_
- Aggiunta personalizzazione della causale della fattura
## 1.6.43
###### _Mag 20, 2019_
- Modifica aspetto pagina impostazioni
- Modifica aspetto risposta verifica API Key
## 1.6.42
###### _Mag 11, 2019_
- Aggiunta la data di scadenza come risposta alla verifica API Key
## 1.6.41
###### _Mag 08, 2019_
- Aggiunta gestione campi PEC e Codice SDI lato admin
## 1.6.40
###### _Apr 16, 2019_
- Modifica aspetto pagina impostazioni
## 1.6.39
###### _Apr 08, 2019_
- Modifica etichette riferimenti normativi
## 1.6.38
###### _Apr 04, 2019_
- Aggiunto pulsante per accedere al gestionale Fattura24 dalle impostazioni
## 1.6.37
###### _Apr 02, 2019_
- Ripristino del pulsante Verifica API KEY
## 1.6.36
###### _Mar 27, 2019_
- Aggiunti riferimenti normativi in calce alle impostazioni
## 1.6.35
###### _Mar 22, 2019_
- Corretto bug sulla Natura Iva: veniva riportata anche su prodotti con aliquota standard
## 1.6.34
###### _Mar 16, 2019_
- Corretto bug sull'indirizzo di spedizione: riportava la provincia dell'indirizzo di fatturazione
## 1.6.33
###### _Mar 11, 2019_
- Cambiata la gestione dei campi SDI e PEC
## 1.6.32
###### _Feb 26, 2019_
- Corretta gestione bonifico bancario e dati IBAN
- correzione bug minori
## 1.6.31
###### _Gen 31, 2019_
- Corretto bug su product_reference
- Disabilitata creazione fatture per ordini a zero
- Salvataggio contemporaneo dei campi pec e sdi (se non vuoti)
- gestione metodi pagamento paypal, assegno e bonifico secondo direttive SdI
## 1.6.30
###### _Gen 15, 2019_
- Aggiunta possibilità di utilizzare i sezionali per la fatturazione elettronica.
- Corretto bug sulla creazione delle ricevute, se gli ordini sono effettuati da clienti privati
## 1.6.29
###### _Dic 14, 2018_
- Corretto ulteriore bug nel front office sul download dei pdf, dopo l'aggiornamento per la fatturazione elettronica
## 1.6.28
###### _Dic 14, 2018_
- Corretto bug sul download dei pdf delle fatture, dopo l'aggiornamento del plugin per la fatturazione elettronica
## 1.6.27
###### _Dic 07, 2018_
- Creato nuovo ramo di sviluppo per abilitare il supporto per la fatturazione elettronica con la versione 1.6 di prestashop (ATTENZIONE: funzionalit� in versione beta)